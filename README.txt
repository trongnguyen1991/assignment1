Change current directory to initial/src where there is file run.py
Type: python run.py gen
Then type: python run.py test LexerSuite
Then type: python run.py test ParserSuite

# Setup environment
cd /usr/local/lib
curl -O https://www.antlr.org/download/antlr-4.7.2-complete.jar
export ANTLR_JAR='/usr/local/lib/antlr-4.7.2-complete.jar'
pip3 install antlr4-python3-runtime
