# Generated from main/mc/parser/MC.g4 by ANTLR 4.7.2
from antlr4 import *
if __name__ is not None and "." in __name__:
    from .MCParser import MCParser
else:
    from MCParser import MCParser

# This class defines a complete generic visitor for a parse tree produced by MCParser.

class MCVisitor(ParseTreeVisitor):

    # Visit a parse tree produced by MCParser#program.
    def visitProgram(self, ctx:MCParser.ProgramContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by MCParser#decl.
    def visitDecl(self, ctx:MCParser.DeclContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by MCParser#funcDecl.
    def visitFuncDecl(self, ctx:MCParser.FuncDeclContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by MCParser#funType.
    def visitFunType(self, ctx:MCParser.FunTypeContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by MCParser#funReturnType.
    def visitFunReturnType(self, ctx:MCParser.FunReturnTypeContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by MCParser#paramList.
    def visitParamList(self, ctx:MCParser.ParamListContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by MCParser#funcParName.
    def visitFuncParName(self, ctx:MCParser.FuncParNameContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by MCParser#varType.
    def visitVarType(self, ctx:MCParser.VarTypeContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by MCParser#block.
    def visitBlock(self, ctx:MCParser.BlockContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by MCParser#varDecl.
    def visitVarDecl(self, ctx:MCParser.VarDeclContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by MCParser#varName.
    def visitVarName(self, ctx:MCParser.VarNameContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by MCParser#stmt.
    def visitStmt(self, ctx:MCParser.StmtContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by MCParser#nonIfStmt.
    def visitNonIfStmt(self, ctx:MCParser.NonIfStmtContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by MCParser#ifelse.
    def visitIfelse(self, ctx:MCParser.IfelseContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by MCParser#match.
    def visitMatch(self, ctx:MCParser.MatchContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by MCParser#unMatch.
    def visitUnMatch(self, ctx:MCParser.UnMatchContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by MCParser#dowhile.
    def visitDowhile(self, ctx:MCParser.DowhileContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by MCParser#forloop.
    def visitForloop(self, ctx:MCParser.ForloopContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by MCParser#dataTypeLit.
    def visitDataTypeLit(self, ctx:MCParser.DataTypeLitContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by MCParser#returnStmt.
    def visitReturnStmt(self, ctx:MCParser.ReturnStmtContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by MCParser#expr.
    def visitExpr(self, ctx:MCParser.ExprContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by MCParser#parExpr.
    def visitParExpr(self, ctx:MCParser.ParExprContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by MCParser#funCall.
    def visitFunCall(self, ctx:MCParser.FunCallContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by MCParser#funCallParams.
    def visitFunCallParams(self, ctx:MCParser.FunCallParamsContext):
        return self.visitChildren(ctx)



del MCParser