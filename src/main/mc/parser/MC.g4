/**
 * Student name: Nguyen Ngoc Trong
 * Student ID: 1770482
 */
grammar MC;

@lexer::header {
from lexererr import *
}

@lexer::members {
def emit(self):
    tk = self.type
    if tk == self.UNCLOSE_STRING:
        result = super().emit();
        raise UncloseString(result.text);
    elif tk == self.ILLEGAL_ESCAPE:
        result = super().emit();
        raise IllegalEscape(result.text);
    elif tk == self.ERROR_CHAR:
        result = super().emit();
        raise ErrorToken(result.text);
    else:
        return super().emit();
}

options{
	language=Python3;
}

program: decl* EOF ;

decl: varDecl | funcDecl;

funcDecl: funType ID paramList block;

funType: funReturnType | VOID;

funReturnType: varType (LS RS)?;

paramList: LB ((varType funcParName) (COMMA varType funcParName)*)? RB;

funcParName: ID(LS RS)?;

varType: INT | STRING | FLOAT | BOOLEAN ;

block: LP (varDecl | stmt)* RP;

varDecl: varType varName (COMMA varName)* SEMI;

varName: ID (LS IntLit RS)?;

stmt: ifelse | nonIfStmt;

nonIfStmt: dowhile | expr SEMI | funCall SEMI | forloop | BREAK SEMI | CONTINUE SEMI | returnStmt | block;

ifelse: matchStmt | unMatchStmt;

matchStmt: IF parExpr (matchStmt | nonIfStmt) ELSE stmt;

unMatchStmt: IF parExpr stmt;

dowhile: DO stmt+ WHILE expr SEMI;

forloop: FOR LB expr SEMI expr SEMI expr RB stmt;

dataTypeLit: IntLit | FloatLit | BoolLit | StrLit ;

returnStmt: RETURN SEMI | RETURN expr SEMI;

expr
    : LB expr RB
    | expr LS expr RS
    | <assoc=right> (SUB | NOT) expr
    | expr (MUL | DIV | MOD) expr
    | expr (ADD | SUB) expr
    | expr (LT | GT | LTEQ | GTEQ) expr
    | expr (EQ | NEQ) expr
    | expr (AND | OR) expr
    | dataTypeLit | ID  | funCall
    | <assoc=right> expr ASIGN expr
    ;

parExpr: LB expr RB;

funCall: ID LB funCallParams RB;

funCallParams: (expr (COMMA expr)*)?;

fragment DIGIT: [0-9];
fragment CHARACTER: [a-zA-Z];
fragment ESCAPE_SEQUENCE: '\\' [btnfr"'\\];

/* Keywords */
BREAK: 'break';
CONTINUE: 'continue';
ELSE: 'else';
FOR: 'for';
IF: 'if';
RETURN: 'return';
VOID: 'void';
DO: 'do';
WHILE: 'while';

/* Types declaration */
BOOLEAN: 'boolean';
FLOAT: 'float';
INT: 'int';
STRING: 'string';

/* Operators */
ADD: '+';
SUB: '-';
MUL: '*';
DIV: '/';
NOT: '!';
MOD: '%';
OR: '||';
AND: '&&';
EQ: '==';
NEQ: '!=';
LT: '<';
GT: '>';
LTEQ: '<=';
GTEQ: '>=';
ASIGN: '=';

/* Separators */
LS: '[';
RS: ']';
LP: '{';
RP: '}';
LB: '(';
RB: ')';
SEMI: ';';
COMMA: ',';

/* Literals */
IntLit: DIGIT+;
FloatLit
    :DIGIT+ '.' DIGIT*
    |DIGIT* '.' DIGIT+
    |DIGIT+ ('e'|'E')[+-]? DIGIT+
    |DIGIT* '.' DIGIT+ ('e'|'E')[+-]? DIGIT+
    ;
BoolLit: 'true' | 'false';

StrLit
    : '"' (~["\\\r\n] | ESCAPE_SEQUENCE)* '"'
    {setText(getText().substring(1, getText().length()-1));}
    ;

/* Comments */
COMMENT: '/*' .*? '*/' -> skip;
LINE_COMMENT: '//' ~[\r\n]* -> skip;

WS : [ \t\r\n\b\f]+ -> skip ; // skip spaces, tabs, newlines

/* Identifier */
ID: ('_'|CHARACTER)(DIGIT|'_'|CHARACTER)*;

ILLEGAL_ESCAPE: '"' ~[\r\n"]* '\\' ~[bfrnt'"\\]* '"' ;

UNCLOSE_STRING:'"' (~'"')* EOF ;

ERROR_CHAR: .;