# Generated from main/mc/parser/MC.g4 by ANTLR 4.7.2
# encoding: utf-8
from antlr4 import *
from io import StringIO
from typing.io import TextIO
import sys


def serializedATN():
    with StringIO() as buf:
        buf.write("\3\u608b\ua72a\u8133\ub9ed\u417c\u3be7\u7786\u5964\3\61")
        buf.write("\u00fe\4\2\t\2\4\3\t\3\4\4\t\4\4\5\t\5\4\6\t\6\4\7\t\7")
        buf.write("\4\b\t\b\4\t\t\t\4\n\t\n\4\13\t\13\4\f\t\f\4\r\t\r\4\16")
        buf.write("\t\16\4\17\t\17\4\20\t\20\4\21\t\21\4\22\t\22\4\23\t\23")
        buf.write("\4\24\t\24\4\25\t\25\4\26\t\26\4\27\t\27\4\30\t\30\4\31")
        buf.write("\t\31\3\2\7\2\64\n\2\f\2\16\2\67\13\2\3\2\3\2\3\3\3\3")
        buf.write("\5\3=\n\3\3\4\3\4\3\4\3\4\3\4\3\5\3\5\5\5F\n\5\3\6\3\6")
        buf.write("\3\6\5\6K\n\6\3\7\3\7\3\7\3\7\3\7\3\7\3\7\3\7\7\7U\n\7")
        buf.write("\f\7\16\7X\13\7\5\7Z\n\7\3\7\3\7\3\b\3\b\3\b\5\ba\n\b")
        buf.write("\3\t\3\t\3\n\3\n\3\n\7\nh\n\n\f\n\16\nk\13\n\3\n\3\n\3")
        buf.write("\13\3\13\3\13\3\13\7\13s\n\13\f\13\16\13v\13\13\3\13\3")
        buf.write("\13\3\f\3\f\3\f\3\f\5\f~\n\f\3\r\3\r\5\r\u0082\n\r\3\16")
        buf.write("\3\16\3\16\3\16\3\16\3\16\3\16\3\16\3\16\3\16\3\16\3\16")
        buf.write("\3\16\3\16\5\16\u0092\n\16\3\17\3\17\5\17\u0096\n\17\3")
        buf.write("\20\3\20\3\20\3\20\5\20\u009c\n\20\3\20\3\20\3\20\3\21")
        buf.write("\3\21\3\21\3\21\3\22\3\22\6\22\u00a7\n\22\r\22\16\22\u00a8")
        buf.write("\3\22\3\22\3\22\3\22\3\23\3\23\3\23\3\23\3\23\3\23\3\23")
        buf.write("\3\23\3\23\3\23\3\24\3\24\3\25\3\25\3\25\3\25\3\25\3\25")
        buf.write("\5\25\u00c1\n\25\3\26\3\26\3\26\3\26\3\26\3\26\3\26\3")
        buf.write("\26\3\26\3\26\5\26\u00cd\n\26\3\26\3\26\3\26\3\26\3\26")
        buf.write("\3\26\3\26\3\26\3\26\3\26\3\26\3\26\3\26\3\26\3\26\3\26")
        buf.write("\3\26\3\26\3\26\3\26\3\26\3\26\3\26\7\26\u00e6\n\26\f")
        buf.write("\26\16\26\u00e9\13\26\3\27\3\27\3\27\3\27\3\30\3\30\3")
        buf.write("\30\3\30\3\30\3\31\3\31\3\31\7\31\u00f7\n\31\f\31\16\31")
        buf.write("\u00fa\13\31\5\31\u00fc\n\31\3\31\2\3*\32\2\4\6\b\n\f")
        buf.write("\16\20\22\24\26\30\32\34\36 \"$&(*,.\60\2\n\3\2\f\17\3")
        buf.write("\2\'*\4\2\21\21\24\24\4\2\22\23\25\25\3\2\20\21\3\2\32")
        buf.write("\35\3\2\30\31\3\2\26\27\2\u0109\2\65\3\2\2\2\4<\3\2\2")
        buf.write("\2\6>\3\2\2\2\bE\3\2\2\2\nG\3\2\2\2\fL\3\2\2\2\16]\3\2")
        buf.write("\2\2\20b\3\2\2\2\22d\3\2\2\2\24n\3\2\2\2\26y\3\2\2\2\30")
        buf.write("\u0081\3\2\2\2\32\u0091\3\2\2\2\34\u0095\3\2\2\2\36\u0097")
        buf.write("\3\2\2\2 \u00a0\3\2\2\2\"\u00a4\3\2\2\2$\u00ae\3\2\2\2")
        buf.write("&\u00b8\3\2\2\2(\u00c0\3\2\2\2*\u00cc\3\2\2\2,\u00ea\3")
        buf.write("\2\2\2.\u00ee\3\2\2\2\60\u00fb\3\2\2\2\62\64\5\4\3\2\63")
        buf.write("\62\3\2\2\2\64\67\3\2\2\2\65\63\3\2\2\2\65\66\3\2\2\2")
        buf.write("\668\3\2\2\2\67\65\3\2\2\289\7\2\2\39\3\3\2\2\2:=\5\24")
        buf.write("\13\2;=\5\6\4\2<:\3\2\2\2<;\3\2\2\2=\5\3\2\2\2>?\5\b\5")
        buf.write("\2?@\7.\2\2@A\5\f\7\2AB\5\22\n\2B\7\3\2\2\2CF\5\n\6\2")
        buf.write("DF\7\t\2\2EC\3\2\2\2ED\3\2\2\2F\t\3\2\2\2GJ\5\20\t\2H")
        buf.write("I\7\37\2\2IK\7 \2\2JH\3\2\2\2JK\3\2\2\2K\13\3\2\2\2LY")
        buf.write("\7#\2\2MN\5\20\t\2NO\5\16\b\2OV\3\2\2\2PQ\7&\2\2QR\5\20")
        buf.write("\t\2RS\5\16\b\2SU\3\2\2\2TP\3\2\2\2UX\3\2\2\2VT\3\2\2")
        buf.write("\2VW\3\2\2\2WZ\3\2\2\2XV\3\2\2\2YM\3\2\2\2YZ\3\2\2\2Z")
        buf.write("[\3\2\2\2[\\\7$\2\2\\\r\3\2\2\2]`\7.\2\2^_\7\37\2\2_a")
        buf.write("\7 \2\2`^\3\2\2\2`a\3\2\2\2a\17\3\2\2\2bc\t\2\2\2c\21")
        buf.write("\3\2\2\2di\7!\2\2eh\5\24\13\2fh\5\30\r\2ge\3\2\2\2gf\3")
        buf.write("\2\2\2hk\3\2\2\2ig\3\2\2\2ij\3\2\2\2jl\3\2\2\2ki\3\2\2")
        buf.write("\2lm\7\"\2\2m\23\3\2\2\2no\5\20\t\2ot\5\26\f\2pq\7&\2")
        buf.write("\2qs\5\26\f\2rp\3\2\2\2sv\3\2\2\2tr\3\2\2\2tu\3\2\2\2")
        buf.write("uw\3\2\2\2vt\3\2\2\2wx\7%\2\2x\25\3\2\2\2y}\7.\2\2z{\7")
        buf.write("\37\2\2{|\7\'\2\2|~\7 \2\2}z\3\2\2\2}~\3\2\2\2~\27\3\2")
        buf.write("\2\2\177\u0082\5\34\17\2\u0080\u0082\5\32\16\2\u0081\177")
        buf.write("\3\2\2\2\u0081\u0080\3\2\2\2\u0082\31\3\2\2\2\u0083\u0092")
        buf.write("\5\"\22\2\u0084\u0085\5*\26\2\u0085\u0086\7%\2\2\u0086")
        buf.write("\u0092\3\2\2\2\u0087\u0088\5.\30\2\u0088\u0089\7%\2\2")
        buf.write("\u0089\u0092\3\2\2\2\u008a\u0092\5$\23\2\u008b\u008c\7")
        buf.write("\3\2\2\u008c\u0092\7%\2\2\u008d\u008e\7\4\2\2\u008e\u0092")
        buf.write("\7%\2\2\u008f\u0092\5(\25\2\u0090\u0092\5\22\n\2\u0091")
        buf.write("\u0083\3\2\2\2\u0091\u0084\3\2\2\2\u0091\u0087\3\2\2\2")
        buf.write("\u0091\u008a\3\2\2\2\u0091\u008b\3\2\2\2\u0091\u008d\3")
        buf.write("\2\2\2\u0091\u008f\3\2\2\2\u0091\u0090\3\2\2\2\u0092\33")
        buf.write("\3\2\2\2\u0093\u0096\5\36\20\2\u0094\u0096\5 \21\2\u0095")
        buf.write("\u0093\3\2\2\2\u0095\u0094\3\2\2\2\u0096\35\3\2\2\2\u0097")
        buf.write("\u0098\7\7\2\2\u0098\u009b\5,\27\2\u0099\u009c\5\36\20")
        buf.write("\2\u009a\u009c\5\32\16\2\u009b\u0099\3\2\2\2\u009b\u009a")
        buf.write("\3\2\2\2\u009c\u009d\3\2\2\2\u009d\u009e\7\5\2\2\u009e")
        buf.write("\u009f\5\30\r\2\u009f\37\3\2\2\2\u00a0\u00a1\7\7\2\2\u00a1")
        buf.write("\u00a2\5,\27\2\u00a2\u00a3\5\30\r\2\u00a3!\3\2\2\2\u00a4")
        buf.write("\u00a6\7\n\2\2\u00a5\u00a7\5\30\r\2\u00a6\u00a5\3\2\2")
        buf.write("\2\u00a7\u00a8\3\2\2\2\u00a8\u00a6\3\2\2\2\u00a8\u00a9")
        buf.write("\3\2\2\2\u00a9\u00aa\3\2\2\2\u00aa\u00ab\7\13\2\2\u00ab")
        buf.write("\u00ac\5*\26\2\u00ac\u00ad\7%\2\2\u00ad#\3\2\2\2\u00ae")
        buf.write("\u00af\7\6\2\2\u00af\u00b0\7#\2\2\u00b0\u00b1\5*\26\2")
        buf.write("\u00b1\u00b2\7%\2\2\u00b2\u00b3\5*\26\2\u00b3\u00b4\7")
        buf.write("%\2\2\u00b4\u00b5\5*\26\2\u00b5\u00b6\7$\2\2\u00b6\u00b7")
        buf.write("\5\30\r\2\u00b7%\3\2\2\2\u00b8\u00b9\t\3\2\2\u00b9\'\3")
        buf.write("\2\2\2\u00ba\u00bb\7\b\2\2\u00bb\u00c1\7%\2\2\u00bc\u00bd")
        buf.write("\7\b\2\2\u00bd\u00be\5*\26\2\u00be\u00bf\7%\2\2\u00bf")
        buf.write("\u00c1\3\2\2\2\u00c0\u00ba\3\2\2\2\u00c0\u00bc\3\2\2\2")
        buf.write("\u00c1)\3\2\2\2\u00c2\u00c3\b\26\1\2\u00c3\u00c4\7#\2")
        buf.write("\2\u00c4\u00c5\5*\26\2\u00c5\u00c6\7$\2\2\u00c6\u00cd")
        buf.write("\3\2\2\2\u00c7\u00c8\t\4\2\2\u00c8\u00cd\5*\26\f\u00c9")
        buf.write("\u00cd\5&\24\2\u00ca\u00cd\7.\2\2\u00cb\u00cd\5.\30\2")
        buf.write("\u00cc\u00c2\3\2\2\2\u00cc\u00c7\3\2\2\2\u00cc\u00c9\3")
        buf.write("\2\2\2\u00cc\u00ca\3\2\2\2\u00cc\u00cb\3\2\2\2\u00cd\u00e7")
        buf.write("\3\2\2\2\u00ce\u00cf\f\13\2\2\u00cf\u00d0\t\5\2\2\u00d0")
        buf.write("\u00e6\5*\26\f\u00d1\u00d2\f\n\2\2\u00d2\u00d3\t\6\2\2")
        buf.write("\u00d3\u00e6\5*\26\13\u00d4\u00d5\f\t\2\2\u00d5\u00d6")
        buf.write("\t\7\2\2\u00d6\u00e6\5*\26\n\u00d7\u00d8\f\b\2\2\u00d8")
        buf.write("\u00d9\t\b\2\2\u00d9\u00e6\5*\26\t\u00da\u00db\f\7\2\2")
        buf.write("\u00db\u00dc\t\t\2\2\u00dc\u00e6\5*\26\b\u00dd\u00de\f")
        buf.write("\3\2\2\u00de\u00df\7\36\2\2\u00df\u00e6\5*\26\3\u00e0")
        buf.write("\u00e1\f\r\2\2\u00e1\u00e2\7\37\2\2\u00e2\u00e3\5*\26")
        buf.write("\2\u00e3\u00e4\7 \2\2\u00e4\u00e6\3\2\2\2\u00e5\u00ce")
        buf.write("\3\2\2\2\u00e5\u00d1\3\2\2\2\u00e5\u00d4\3\2\2\2\u00e5")
        buf.write("\u00d7\3\2\2\2\u00e5\u00da\3\2\2\2\u00e5\u00dd\3\2\2\2")
        buf.write("\u00e5\u00e0\3\2\2\2\u00e6\u00e9\3\2\2\2\u00e7\u00e5\3")
        buf.write("\2\2\2\u00e7\u00e8\3\2\2\2\u00e8+\3\2\2\2\u00e9\u00e7")
        buf.write("\3\2\2\2\u00ea\u00eb\7#\2\2\u00eb\u00ec\5*\26\2\u00ec")
        buf.write("\u00ed\7$\2\2\u00ed-\3\2\2\2\u00ee\u00ef\7.\2\2\u00ef")
        buf.write("\u00f0\7#\2\2\u00f0\u00f1\5\60\31\2\u00f1\u00f2\7$\2\2")
        buf.write("\u00f2/\3\2\2\2\u00f3\u00f8\5*\26\2\u00f4\u00f5\7&\2\2")
        buf.write("\u00f5\u00f7\5*\26\2\u00f6\u00f4\3\2\2\2\u00f7\u00fa\3")
        buf.write("\2\2\2\u00f8\u00f6\3\2\2\2\u00f8\u00f9\3\2\2\2\u00f9\u00fc")
        buf.write("\3\2\2\2\u00fa\u00f8\3\2\2\2\u00fb\u00f3\3\2\2\2\u00fb")
        buf.write("\u00fc\3\2\2\2\u00fc\61\3\2\2\2\30\65<EJVY`git}\u0081")
        buf.write("\u0091\u0095\u009b\u00a8\u00c0\u00cc\u00e5\u00e7\u00f8")
        buf.write("\u00fb")
        return buf.getvalue()


class MCParser ( Parser ):

    grammarFileName = "MC.g4"

    atn = ATNDeserializer().deserialize(serializedATN())

    decisionsToDFA = [ DFA(ds, i) for i, ds in enumerate(atn.decisionToState) ]

    sharedContextCache = PredictionContextCache()

    literalNames = [ "<INVALID>", "'break'", "'continue'", "'else'", "'for'", 
                     "'if'", "'return'", "'void'", "'do'", "'while'", "'boolean'", 
                     "'float'", "'int'", "'string'", "'+'", "'-'", "'*'", 
                     "'/'", "'!'", "'%'", "'||'", "'&&'", "'=='", "'!='", 
                     "'<'", "'>'", "'<='", "'>='", "'='", "'['", "']'", 
                     "'{'", "'}'", "'('", "')'", "';'", "','" ]

    symbolicNames = [ "<INVALID>", "BREAK", "CONTINUE", "ELSE", "FOR", "IF", 
                      "RETURN", "VOID", "DO", "WHILE", "BOOLEAN", "FLOAT", 
                      "INT", "STRING", "ADD", "SUB", "MUL", "DIV", "NOT", 
                      "MOD", "OR", "AND", "EQ", "NEQ", "LT", "GT", "LTEQ", 
                      "GTEQ", "ASIGN", "LS", "RS", "LP", "RP", "LB", "RB", 
                      "SEMI", "COMMA", "IntLit", "FloatLit", "BoolLit", 
                      "StrLit", "COMMENT", "LINE_COMMENT", "WS", "ID", "ILLEGAL_ESCAPE", 
                      "UNCLOSE_STRING", "ERROR_CHAR" ]

    RULE_program = 0
    RULE_decl = 1
    RULE_funcDecl = 2
    RULE_funType = 3
    RULE_funReturnType = 4
    RULE_paramList = 5
    RULE_funcParName = 6
    RULE_varType = 7
    RULE_block = 8
    RULE_varDecl = 9
    RULE_varName = 10
    RULE_stmt = 11
    RULE_nonIfStmt = 12
    RULE_ifelse = 13
    RULE_match = 14
    RULE_unMatch = 15
    RULE_dowhile = 16
    RULE_forloop = 17
    RULE_dataTypeLit = 18
    RULE_returnStmt = 19
    RULE_expr = 20
    RULE_parExpr = 21
    RULE_funCall = 22
    RULE_funCallParams = 23

    ruleNames =  [ "program", "decl", "funcDecl", "funType", "funReturnType", 
                   "paramList", "funcParName", "varType", "block", "varDecl", 
                   "varName", "stmt", "nonIfStmt", "ifelse", "match", "unMatch", 
                   "dowhile", "forloop", "dataTypeLit", "returnStmt", "expr", 
                   "parExpr", "funCall", "funCallParams" ]

    EOF = Token.EOF
    BREAK=1
    CONTINUE=2
    ELSE=3
    FOR=4
    IF=5
    RETURN=6
    VOID=7
    DO=8
    WHILE=9
    BOOLEAN=10
    FLOAT=11
    INT=12
    STRING=13
    ADD=14
    SUB=15
    MUL=16
    DIV=17
    NOT=18
    MOD=19
    OR=20
    AND=21
    EQ=22
    NEQ=23
    LT=24
    GT=25
    LTEQ=26
    GTEQ=27
    ASIGN=28
    LS=29
    RS=30
    LP=31
    RP=32
    LB=33
    RB=34
    SEMI=35
    COMMA=36
    IntLit=37
    FloatLit=38
    BoolLit=39
    StrLit=40
    COMMENT=41
    LINE_COMMENT=42
    WS=43
    ID=44
    ILLEGAL_ESCAPE=45
    UNCLOSE_STRING=46
    ERROR_CHAR=47

    def __init__(self, input:TokenStream, output:TextIO = sys.stdout):
        super().__init__(input, output)
        self.checkVersion("4.7.2")
        self._interp = ParserATNSimulator(self, self.atn, self.decisionsToDFA, self.sharedContextCache)
        self._predicates = None




    class ProgramContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def EOF(self):
            return self.getToken(MCParser.EOF, 0)

        def decl(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(MCParser.DeclContext)
            else:
                return self.getTypedRuleContext(MCParser.DeclContext,i)


        def getRuleIndex(self):
            return MCParser.RULE_program

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitProgram" ):
                return visitor.visitProgram(self)
            else:
                return visitor.visitChildren(self)




    def program(self):

        localctx = MCParser.ProgramContext(self, self._ctx, self.state)
        self.enterRule(localctx, 0, self.RULE_program)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 51
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            while (((_la) & ~0x3f) == 0 and ((1 << _la) & ((1 << MCParser.VOID) | (1 << MCParser.BOOLEAN) | (1 << MCParser.FLOAT) | (1 << MCParser.INT) | (1 << MCParser.STRING))) != 0):
                self.state = 48
                self.decl()
                self.state = 53
                self._errHandler.sync(self)
                _la = self._input.LA(1)

            self.state = 54
            self.match(MCParser.EOF)
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class DeclContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def varDecl(self):
            return self.getTypedRuleContext(MCParser.VarDeclContext,0)


        def funcDecl(self):
            return self.getTypedRuleContext(MCParser.FuncDeclContext,0)


        def getRuleIndex(self):
            return MCParser.RULE_decl

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitDecl" ):
                return visitor.visitDecl(self)
            else:
                return visitor.visitChildren(self)




    def decl(self):

        localctx = MCParser.DeclContext(self, self._ctx, self.state)
        self.enterRule(localctx, 2, self.RULE_decl)
        try:
            self.state = 58
            self._errHandler.sync(self)
            la_ = self._interp.adaptivePredict(self._input,1,self._ctx)
            if la_ == 1:
                self.enterOuterAlt(localctx, 1)
                self.state = 56
                self.varDecl()
                pass

            elif la_ == 2:
                self.enterOuterAlt(localctx, 2)
                self.state = 57
                self.funcDecl()
                pass


        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class FuncDeclContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def funType(self):
            return self.getTypedRuleContext(MCParser.FunTypeContext,0)


        def ID(self):
            return self.getToken(MCParser.ID, 0)

        def paramList(self):
            return self.getTypedRuleContext(MCParser.ParamListContext,0)


        def block(self):
            return self.getTypedRuleContext(MCParser.BlockContext,0)


        def getRuleIndex(self):
            return MCParser.RULE_funcDecl

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitFuncDecl" ):
                return visitor.visitFuncDecl(self)
            else:
                return visitor.visitChildren(self)




    def funcDecl(self):

        localctx = MCParser.FuncDeclContext(self, self._ctx, self.state)
        self.enterRule(localctx, 4, self.RULE_funcDecl)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 60
            self.funType()
            self.state = 61
            self.match(MCParser.ID)
            self.state = 62
            self.paramList()
            self.state = 63
            self.block()
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class FunTypeContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def funReturnType(self):
            return self.getTypedRuleContext(MCParser.FunReturnTypeContext,0)


        def VOID(self):
            return self.getToken(MCParser.VOID, 0)

        def getRuleIndex(self):
            return MCParser.RULE_funType

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitFunType" ):
                return visitor.visitFunType(self)
            else:
                return visitor.visitChildren(self)




    def funType(self):

        localctx = MCParser.FunTypeContext(self, self._ctx, self.state)
        self.enterRule(localctx, 6, self.RULE_funType)
        try:
            self.state = 67
            self._errHandler.sync(self)
            token = self._input.LA(1)
            if token in [MCParser.BOOLEAN, MCParser.FLOAT, MCParser.INT, MCParser.STRING]:
                self.enterOuterAlt(localctx, 1)
                self.state = 65
                self.funReturnType()
                pass
            elif token in [MCParser.VOID]:
                self.enterOuterAlt(localctx, 2)
                self.state = 66
                self.match(MCParser.VOID)
                pass
            else:
                raise NoViableAltException(self)

        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class FunReturnTypeContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def varType(self):
            return self.getTypedRuleContext(MCParser.VarTypeContext,0)


        def LS(self):
            return self.getToken(MCParser.LS, 0)

        def RS(self):
            return self.getToken(MCParser.RS, 0)

        def getRuleIndex(self):
            return MCParser.RULE_funReturnType

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitFunReturnType" ):
                return visitor.visitFunReturnType(self)
            else:
                return visitor.visitChildren(self)




    def funReturnType(self):

        localctx = MCParser.FunReturnTypeContext(self, self._ctx, self.state)
        self.enterRule(localctx, 8, self.RULE_funReturnType)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 69
            self.varType()
            self.state = 72
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            if _la==MCParser.LS:
                self.state = 70
                self.match(MCParser.LS)
                self.state = 71
                self.match(MCParser.RS)


        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class ParamListContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def LB(self):
            return self.getToken(MCParser.LB, 0)

        def RB(self):
            return self.getToken(MCParser.RB, 0)

        def varType(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(MCParser.VarTypeContext)
            else:
                return self.getTypedRuleContext(MCParser.VarTypeContext,i)


        def funcParName(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(MCParser.FuncParNameContext)
            else:
                return self.getTypedRuleContext(MCParser.FuncParNameContext,i)


        def COMMA(self, i:int=None):
            if i is None:
                return self.getTokens(MCParser.COMMA)
            else:
                return self.getToken(MCParser.COMMA, i)

        def getRuleIndex(self):
            return MCParser.RULE_paramList

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitParamList" ):
                return visitor.visitParamList(self)
            else:
                return visitor.visitChildren(self)




    def paramList(self):

        localctx = MCParser.ParamListContext(self, self._ctx, self.state)
        self.enterRule(localctx, 10, self.RULE_paramList)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 74
            self.match(MCParser.LB)
            self.state = 87
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            if (((_la) & ~0x3f) == 0 and ((1 << _la) & ((1 << MCParser.BOOLEAN) | (1 << MCParser.FLOAT) | (1 << MCParser.INT) | (1 << MCParser.STRING))) != 0):
                self.state = 75
                self.varType()
                self.state = 76
                self.funcParName()
                self.state = 84
                self._errHandler.sync(self)
                _la = self._input.LA(1)
                while _la==MCParser.COMMA:
                    self.state = 78
                    self.match(MCParser.COMMA)
                    self.state = 79
                    self.varType()
                    self.state = 80
                    self.funcParName()
                    self.state = 86
                    self._errHandler.sync(self)
                    _la = self._input.LA(1)



            self.state = 89
            self.match(MCParser.RB)
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class FuncParNameContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def ID(self):
            return self.getToken(MCParser.ID, 0)

        def LS(self):
            return self.getToken(MCParser.LS, 0)

        def RS(self):
            return self.getToken(MCParser.RS, 0)

        def getRuleIndex(self):
            return MCParser.RULE_funcParName

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitFuncParName" ):
                return visitor.visitFuncParName(self)
            else:
                return visitor.visitChildren(self)




    def funcParName(self):

        localctx = MCParser.FuncParNameContext(self, self._ctx, self.state)
        self.enterRule(localctx, 12, self.RULE_funcParName)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 91
            self.match(MCParser.ID)
            self.state = 94
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            if _la==MCParser.LS:
                self.state = 92
                self.match(MCParser.LS)
                self.state = 93
                self.match(MCParser.RS)


        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class VarTypeContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def INT(self):
            return self.getToken(MCParser.INT, 0)

        def STRING(self):
            return self.getToken(MCParser.STRING, 0)

        def FLOAT(self):
            return self.getToken(MCParser.FLOAT, 0)

        def BOOLEAN(self):
            return self.getToken(MCParser.BOOLEAN, 0)

        def getRuleIndex(self):
            return MCParser.RULE_varType

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitVarType" ):
                return visitor.visitVarType(self)
            else:
                return visitor.visitChildren(self)




    def varType(self):

        localctx = MCParser.VarTypeContext(self, self._ctx, self.state)
        self.enterRule(localctx, 14, self.RULE_varType)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 96
            _la = self._input.LA(1)
            if not((((_la) & ~0x3f) == 0 and ((1 << _la) & ((1 << MCParser.BOOLEAN) | (1 << MCParser.FLOAT) | (1 << MCParser.INT) | (1 << MCParser.STRING))) != 0)):
                self._errHandler.recoverInline(self)
            else:
                self._errHandler.reportMatch(self)
                self.consume()
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class BlockContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def LP(self):
            return self.getToken(MCParser.LP, 0)

        def RP(self):
            return self.getToken(MCParser.RP, 0)

        def varDecl(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(MCParser.VarDeclContext)
            else:
                return self.getTypedRuleContext(MCParser.VarDeclContext,i)


        def stmt(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(MCParser.StmtContext)
            else:
                return self.getTypedRuleContext(MCParser.StmtContext,i)


        def getRuleIndex(self):
            return MCParser.RULE_block

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitBlock" ):
                return visitor.visitBlock(self)
            else:
                return visitor.visitChildren(self)




    def block(self):

        localctx = MCParser.BlockContext(self, self._ctx, self.state)
        self.enterRule(localctx, 16, self.RULE_block)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 98
            self.match(MCParser.LP)
            self.state = 103
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            while (((_la) & ~0x3f) == 0 and ((1 << _la) & ((1 << MCParser.BREAK) | (1 << MCParser.CONTINUE) | (1 << MCParser.FOR) | (1 << MCParser.IF) | (1 << MCParser.RETURN) | (1 << MCParser.DO) | (1 << MCParser.BOOLEAN) | (1 << MCParser.FLOAT) | (1 << MCParser.INT) | (1 << MCParser.STRING) | (1 << MCParser.SUB) | (1 << MCParser.NOT) | (1 << MCParser.LP) | (1 << MCParser.LB) | (1 << MCParser.IntLit) | (1 << MCParser.FloatLit) | (1 << MCParser.BoolLit) | (1 << MCParser.StrLit) | (1 << MCParser.ID))) != 0):
                self.state = 101
                self._errHandler.sync(self)
                token = self._input.LA(1)
                if token in [MCParser.BOOLEAN, MCParser.FLOAT, MCParser.INT, MCParser.STRING]:
                    self.state = 99
                    self.varDecl()
                    pass
                elif token in [MCParser.BREAK, MCParser.CONTINUE, MCParser.FOR, MCParser.IF, MCParser.RETURN, MCParser.DO, MCParser.SUB, MCParser.NOT, MCParser.LP, MCParser.LB, MCParser.IntLit, MCParser.FloatLit, MCParser.BoolLit, MCParser.StrLit, MCParser.ID]:
                    self.state = 100
                    self.stmt()
                    pass
                else:
                    raise NoViableAltException(self)

                self.state = 105
                self._errHandler.sync(self)
                _la = self._input.LA(1)

            self.state = 106
            self.match(MCParser.RP)
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class VarDeclContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def varType(self):
            return self.getTypedRuleContext(MCParser.VarTypeContext,0)


        def varName(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(MCParser.VarNameContext)
            else:
                return self.getTypedRuleContext(MCParser.VarNameContext,i)


        def SEMI(self):
            return self.getToken(MCParser.SEMI, 0)

        def COMMA(self, i:int=None):
            if i is None:
                return self.getTokens(MCParser.COMMA)
            else:
                return self.getToken(MCParser.COMMA, i)

        def getRuleIndex(self):
            return MCParser.RULE_varDecl

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitVarDecl" ):
                return visitor.visitVarDecl(self)
            else:
                return visitor.visitChildren(self)




    def varDecl(self):

        localctx = MCParser.VarDeclContext(self, self._ctx, self.state)
        self.enterRule(localctx, 18, self.RULE_varDecl)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 108
            self.varType()
            self.state = 109
            self.varName()
            self.state = 114
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            while _la==MCParser.COMMA:
                self.state = 110
                self.match(MCParser.COMMA)
                self.state = 111
                self.varName()
                self.state = 116
                self._errHandler.sync(self)
                _la = self._input.LA(1)

            self.state = 117
            self.match(MCParser.SEMI)
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class VarNameContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def ID(self):
            return self.getToken(MCParser.ID, 0)

        def LS(self):
            return self.getToken(MCParser.LS, 0)

        def IntLit(self):
            return self.getToken(MCParser.IntLit, 0)

        def RS(self):
            return self.getToken(MCParser.RS, 0)

        def getRuleIndex(self):
            return MCParser.RULE_varName

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitVarName" ):
                return visitor.visitVarName(self)
            else:
                return visitor.visitChildren(self)




    def varName(self):

        localctx = MCParser.VarNameContext(self, self._ctx, self.state)
        self.enterRule(localctx, 20, self.RULE_varName)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 119
            self.match(MCParser.ID)
            self.state = 123
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            if _la==MCParser.LS:
                self.state = 120
                self.match(MCParser.LS)
                self.state = 121
                self.match(MCParser.IntLit)
                self.state = 122
                self.match(MCParser.RS)


        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class StmtContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def ifelse(self):
            return self.getTypedRuleContext(MCParser.IfelseContext,0)


        def nonIfStmt(self):
            return self.getTypedRuleContext(MCParser.NonIfStmtContext,0)


        def getRuleIndex(self):
            return MCParser.RULE_stmt

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitStmt" ):
                return visitor.visitStmt(self)
            else:
                return visitor.visitChildren(self)




    def stmt(self):

        localctx = MCParser.StmtContext(self, self._ctx, self.state)
        self.enterRule(localctx, 22, self.RULE_stmt)
        try:
            self.state = 127
            self._errHandler.sync(self)
            token = self._input.LA(1)
            if token in [MCParser.IF]:
                self.enterOuterAlt(localctx, 1)
                self.state = 125
                self.ifelse()
                pass
            elif token in [MCParser.BREAK, MCParser.CONTINUE, MCParser.FOR, MCParser.RETURN, MCParser.DO, MCParser.SUB, MCParser.NOT, MCParser.LP, MCParser.LB, MCParser.IntLit, MCParser.FloatLit, MCParser.BoolLit, MCParser.StrLit, MCParser.ID]:
                self.enterOuterAlt(localctx, 2)
                self.state = 126
                self.nonIfStmt()
                pass
            else:
                raise NoViableAltException(self)

        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class NonIfStmtContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def dowhile(self):
            return self.getTypedRuleContext(MCParser.DowhileContext,0)


        def expr(self):
            return self.getTypedRuleContext(MCParser.ExprContext,0)


        def SEMI(self):
            return self.getToken(MCParser.SEMI, 0)

        def funCall(self):
            return self.getTypedRuleContext(MCParser.FunCallContext,0)


        def forloop(self):
            return self.getTypedRuleContext(MCParser.ForloopContext,0)


        def BREAK(self):
            return self.getToken(MCParser.BREAK, 0)

        def CONTINUE(self):
            return self.getToken(MCParser.CONTINUE, 0)

        def returnStmt(self):
            return self.getTypedRuleContext(MCParser.ReturnStmtContext,0)


        def block(self):
            return self.getTypedRuleContext(MCParser.BlockContext,0)


        def getRuleIndex(self):
            return MCParser.RULE_nonIfStmt

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitNonIfStmt" ):
                return visitor.visitNonIfStmt(self)
            else:
                return visitor.visitChildren(self)




    def nonIfStmt(self):

        localctx = MCParser.NonIfStmtContext(self, self._ctx, self.state)
        self.enterRule(localctx, 24, self.RULE_nonIfStmt)
        try:
            self.state = 143
            self._errHandler.sync(self)
            la_ = self._interp.adaptivePredict(self._input,12,self._ctx)
            if la_ == 1:
                self.enterOuterAlt(localctx, 1)
                self.state = 129
                self.dowhile()
                pass

            elif la_ == 2:
                self.enterOuterAlt(localctx, 2)
                self.state = 130
                self.expr(0)
                self.state = 131
                self.match(MCParser.SEMI)
                pass

            elif la_ == 3:
                self.enterOuterAlt(localctx, 3)
                self.state = 133
                self.funCall()
                self.state = 134
                self.match(MCParser.SEMI)
                pass

            elif la_ == 4:
                self.enterOuterAlt(localctx, 4)
                self.state = 136
                self.forloop()
                pass

            elif la_ == 5:
                self.enterOuterAlt(localctx, 5)
                self.state = 137
                self.match(MCParser.BREAK)
                self.state = 138
                self.match(MCParser.SEMI)
                pass

            elif la_ == 6:
                self.enterOuterAlt(localctx, 6)
                self.state = 139
                self.match(MCParser.CONTINUE)
                self.state = 140
                self.match(MCParser.SEMI)
                pass

            elif la_ == 7:
                self.enterOuterAlt(localctx, 7)
                self.state = 141
                self.returnStmt()
                pass

            elif la_ == 8:
                self.enterOuterAlt(localctx, 8)
                self.state = 142
                self.block()
                pass


        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class IfelseContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def match(self):
            return self.getTypedRuleContext(MCParser.MatchContext,0)


        def unMatch(self):
            return self.getTypedRuleContext(MCParser.UnMatchContext,0)


        def getRuleIndex(self):
            return MCParser.RULE_ifelse

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitIfelse" ):
                return visitor.visitIfelse(self)
            else:
                return visitor.visitChildren(self)




    def ifelse(self):

        localctx = MCParser.IfelseContext(self, self._ctx, self.state)
        self.enterRule(localctx, 26, self.RULE_ifelse)
        try:
            self.state = 147
            self._errHandler.sync(self)
            la_ = self._interp.adaptivePredict(self._input,13,self._ctx)
            if la_ == 1:
                self.enterOuterAlt(localctx, 1)
                self.state = 145
                self.match()
                pass

            elif la_ == 2:
                self.enterOuterAlt(localctx, 2)
                self.state = 146
                self.unMatch()
                pass


        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class MatchContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def IF(self):
            return self.getToken(MCParser.IF, 0)

        def parExpr(self):
            return self.getTypedRuleContext(MCParser.ParExprContext,0)


        def ELSE(self):
            return self.getToken(MCParser.ELSE, 0)

        def stmt(self):
            return self.getTypedRuleContext(MCParser.StmtContext,0)


        def match(self):
            return self.getTypedRuleContext(MCParser.MatchContext,0)


        def nonIfStmt(self):
            return self.getTypedRuleContext(MCParser.NonIfStmtContext,0)


        def getRuleIndex(self):
            return MCParser.RULE_match

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitMatch" ):
                return visitor.visitMatch(self)
            else:
                return visitor.visitChildren(self)




    def match(self):

        localctx = MCParser.MatchContext(self, self._ctx, self.state)
        self.enterRule(localctx, 28, self.RULE_match)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 149
            self.match(MCParser.IF)
            self.state = 150
            self.parExpr()
            self.state = 153
            self._errHandler.sync(self)
            token = self._input.LA(1)
            if token in [MCParser.IF]:
                self.state = 151
                self.match()
                pass
            elif token in [MCParser.BREAK, MCParser.CONTINUE, MCParser.FOR, MCParser.RETURN, MCParser.DO, MCParser.SUB, MCParser.NOT, MCParser.LP, MCParser.LB, MCParser.IntLit, MCParser.FloatLit, MCParser.BoolLit, MCParser.StrLit, MCParser.ID]:
                self.state = 152
                self.nonIfStmt()
                pass
            else:
                raise NoViableAltException(self)

            self.state = 155
            self.match(MCParser.ELSE)
            self.state = 156
            self.stmt()
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class UnMatchContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def IF(self):
            return self.getToken(MCParser.IF, 0)

        def parExpr(self):
            return self.getTypedRuleContext(MCParser.ParExprContext,0)


        def stmt(self):
            return self.getTypedRuleContext(MCParser.StmtContext,0)


        def getRuleIndex(self):
            return MCParser.RULE_unMatch

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitUnMatch" ):
                return visitor.visitUnMatch(self)
            else:
                return visitor.visitChildren(self)




    def unMatch(self):

        localctx = MCParser.UnMatchContext(self, self._ctx, self.state)
        self.enterRule(localctx, 30, self.RULE_unMatch)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 158
            self.match(MCParser.IF)
            self.state = 159
            self.parExpr()
            self.state = 160
            self.stmt()
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class DowhileContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def DO(self):
            return self.getToken(MCParser.DO, 0)

        def WHILE(self):
            return self.getToken(MCParser.WHILE, 0)

        def expr(self):
            return self.getTypedRuleContext(MCParser.ExprContext,0)


        def SEMI(self):
            return self.getToken(MCParser.SEMI, 0)

        def stmt(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(MCParser.StmtContext)
            else:
                return self.getTypedRuleContext(MCParser.StmtContext,i)


        def getRuleIndex(self):
            return MCParser.RULE_dowhile

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitDowhile" ):
                return visitor.visitDowhile(self)
            else:
                return visitor.visitChildren(self)




    def dowhile(self):

        localctx = MCParser.DowhileContext(self, self._ctx, self.state)
        self.enterRule(localctx, 32, self.RULE_dowhile)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 162
            self.match(MCParser.DO)
            self.state = 164 
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            while True:
                self.state = 163
                self.stmt()
                self.state = 166 
                self._errHandler.sync(self)
                _la = self._input.LA(1)
                if not ((((_la) & ~0x3f) == 0 and ((1 << _la) & ((1 << MCParser.BREAK) | (1 << MCParser.CONTINUE) | (1 << MCParser.FOR) | (1 << MCParser.IF) | (1 << MCParser.RETURN) | (1 << MCParser.DO) | (1 << MCParser.SUB) | (1 << MCParser.NOT) | (1 << MCParser.LP) | (1 << MCParser.LB) | (1 << MCParser.IntLit) | (1 << MCParser.FloatLit) | (1 << MCParser.BoolLit) | (1 << MCParser.StrLit) | (1 << MCParser.ID))) != 0)):
                    break

            self.state = 168
            self.match(MCParser.WHILE)
            self.state = 169
            self.expr(0)
            self.state = 170
            self.match(MCParser.SEMI)
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class ForloopContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def FOR(self):
            return self.getToken(MCParser.FOR, 0)

        def LB(self):
            return self.getToken(MCParser.LB, 0)

        def expr(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(MCParser.ExprContext)
            else:
                return self.getTypedRuleContext(MCParser.ExprContext,i)


        def SEMI(self, i:int=None):
            if i is None:
                return self.getTokens(MCParser.SEMI)
            else:
                return self.getToken(MCParser.SEMI, i)

        def RB(self):
            return self.getToken(MCParser.RB, 0)

        def stmt(self):
            return self.getTypedRuleContext(MCParser.StmtContext,0)


        def getRuleIndex(self):
            return MCParser.RULE_forloop

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitForloop" ):
                return visitor.visitForloop(self)
            else:
                return visitor.visitChildren(self)




    def forloop(self):

        localctx = MCParser.ForloopContext(self, self._ctx, self.state)
        self.enterRule(localctx, 34, self.RULE_forloop)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 172
            self.match(MCParser.FOR)
            self.state = 173
            self.match(MCParser.LB)
            self.state = 174
            self.expr(0)
            self.state = 175
            self.match(MCParser.SEMI)
            self.state = 176
            self.expr(0)
            self.state = 177
            self.match(MCParser.SEMI)
            self.state = 178
            self.expr(0)
            self.state = 179
            self.match(MCParser.RB)
            self.state = 180
            self.stmt()
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class DataTypeLitContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def IntLit(self):
            return self.getToken(MCParser.IntLit, 0)

        def FloatLit(self):
            return self.getToken(MCParser.FloatLit, 0)

        def BoolLit(self):
            return self.getToken(MCParser.BoolLit, 0)

        def StrLit(self):
            return self.getToken(MCParser.StrLit, 0)

        def getRuleIndex(self):
            return MCParser.RULE_dataTypeLit

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitDataTypeLit" ):
                return visitor.visitDataTypeLit(self)
            else:
                return visitor.visitChildren(self)




    def dataTypeLit(self):

        localctx = MCParser.DataTypeLitContext(self, self._ctx, self.state)
        self.enterRule(localctx, 36, self.RULE_dataTypeLit)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 182
            _la = self._input.LA(1)
            if not((((_la) & ~0x3f) == 0 and ((1 << _la) & ((1 << MCParser.IntLit) | (1 << MCParser.FloatLit) | (1 << MCParser.BoolLit) | (1 << MCParser.StrLit))) != 0)):
                self._errHandler.recoverInline(self)
            else:
                self._errHandler.reportMatch(self)
                self.consume()
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class ReturnStmtContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def RETURN(self):
            return self.getToken(MCParser.RETURN, 0)

        def SEMI(self):
            return self.getToken(MCParser.SEMI, 0)

        def expr(self):
            return self.getTypedRuleContext(MCParser.ExprContext,0)


        def getRuleIndex(self):
            return MCParser.RULE_returnStmt

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitReturnStmt" ):
                return visitor.visitReturnStmt(self)
            else:
                return visitor.visitChildren(self)




    def returnStmt(self):

        localctx = MCParser.ReturnStmtContext(self, self._ctx, self.state)
        self.enterRule(localctx, 38, self.RULE_returnStmt)
        try:
            self.state = 190
            self._errHandler.sync(self)
            la_ = self._interp.adaptivePredict(self._input,16,self._ctx)
            if la_ == 1:
                self.enterOuterAlt(localctx, 1)
                self.state = 184
                self.match(MCParser.RETURN)
                self.state = 185
                self.match(MCParser.SEMI)
                pass

            elif la_ == 2:
                self.enterOuterAlt(localctx, 2)
                self.state = 186
                self.match(MCParser.RETURN)
                self.state = 187
                self.expr(0)
                self.state = 188
                self.match(MCParser.SEMI)
                pass


        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class ExprContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def LB(self):
            return self.getToken(MCParser.LB, 0)

        def expr(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(MCParser.ExprContext)
            else:
                return self.getTypedRuleContext(MCParser.ExprContext,i)


        def RB(self):
            return self.getToken(MCParser.RB, 0)

        def SUB(self):
            return self.getToken(MCParser.SUB, 0)

        def NOT(self):
            return self.getToken(MCParser.NOT, 0)

        def dataTypeLit(self):
            return self.getTypedRuleContext(MCParser.DataTypeLitContext,0)


        def ID(self):
            return self.getToken(MCParser.ID, 0)

        def funCall(self):
            return self.getTypedRuleContext(MCParser.FunCallContext,0)


        def MUL(self):
            return self.getToken(MCParser.MUL, 0)

        def DIV(self):
            return self.getToken(MCParser.DIV, 0)

        def MOD(self):
            return self.getToken(MCParser.MOD, 0)

        def ADD(self):
            return self.getToken(MCParser.ADD, 0)

        def LT(self):
            return self.getToken(MCParser.LT, 0)

        def GT(self):
            return self.getToken(MCParser.GT, 0)

        def LTEQ(self):
            return self.getToken(MCParser.LTEQ, 0)

        def GTEQ(self):
            return self.getToken(MCParser.GTEQ, 0)

        def EQ(self):
            return self.getToken(MCParser.EQ, 0)

        def NEQ(self):
            return self.getToken(MCParser.NEQ, 0)

        def AND(self):
            return self.getToken(MCParser.AND, 0)

        def OR(self):
            return self.getToken(MCParser.OR, 0)

        def ASIGN(self):
            return self.getToken(MCParser.ASIGN, 0)

        def LS(self):
            return self.getToken(MCParser.LS, 0)

        def RS(self):
            return self.getToken(MCParser.RS, 0)

        def getRuleIndex(self):
            return MCParser.RULE_expr

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitExpr" ):
                return visitor.visitExpr(self)
            else:
                return visitor.visitChildren(self)



    def expr(self, _p:int=0):
        _parentctx = self._ctx
        _parentState = self.state
        localctx = MCParser.ExprContext(self, self._ctx, _parentState)
        _prevctx = localctx
        _startState = 40
        self.enterRecursionRule(localctx, 40, self.RULE_expr, _p)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 202
            self._errHandler.sync(self)
            la_ = self._interp.adaptivePredict(self._input,17,self._ctx)
            if la_ == 1:
                self.state = 193
                self.match(MCParser.LB)
                self.state = 194
                self.expr(0)
                self.state = 195
                self.match(MCParser.RB)
                pass

            elif la_ == 2:
                self.state = 197
                _la = self._input.LA(1)
                if not(_la==MCParser.SUB or _la==MCParser.NOT):
                    self._errHandler.recoverInline(self)
                else:
                    self._errHandler.reportMatch(self)
                    self.consume()
                self.state = 198
                self.expr(10)
                pass

            elif la_ == 3:
                self.state = 199
                self.dataTypeLit()
                pass

            elif la_ == 4:
                self.state = 200
                self.match(MCParser.ID)
                pass

            elif la_ == 5:
                self.state = 201
                self.funCall()
                pass


            self._ctx.stop = self._input.LT(-1)
            self.state = 229
            self._errHandler.sync(self)
            _alt = self._interp.adaptivePredict(self._input,19,self._ctx)
            while _alt!=2 and _alt!=ATN.INVALID_ALT_NUMBER:
                if _alt==1:
                    if self._parseListeners is not None:
                        self.triggerExitRuleEvent()
                    _prevctx = localctx
                    self.state = 227
                    self._errHandler.sync(self)
                    la_ = self._interp.adaptivePredict(self._input,18,self._ctx)
                    if la_ == 1:
                        localctx = MCParser.ExprContext(self, _parentctx, _parentState)
                        self.pushNewRecursionContext(localctx, _startState, self.RULE_expr)
                        self.state = 204
                        if not self.precpred(self._ctx, 9):
                            from antlr4.error.Errors import FailedPredicateException
                            raise FailedPredicateException(self, "self.precpred(self._ctx, 9)")
                        self.state = 205
                        _la = self._input.LA(1)
                        if not((((_la) & ~0x3f) == 0 and ((1 << _la) & ((1 << MCParser.MUL) | (1 << MCParser.DIV) | (1 << MCParser.MOD))) != 0)):
                            self._errHandler.recoverInline(self)
                        else:
                            self._errHandler.reportMatch(self)
                            self.consume()
                        self.state = 206
                        self.expr(10)
                        pass

                    elif la_ == 2:
                        localctx = MCParser.ExprContext(self, _parentctx, _parentState)
                        self.pushNewRecursionContext(localctx, _startState, self.RULE_expr)
                        self.state = 207
                        if not self.precpred(self._ctx, 8):
                            from antlr4.error.Errors import FailedPredicateException
                            raise FailedPredicateException(self, "self.precpred(self._ctx, 8)")
                        self.state = 208
                        _la = self._input.LA(1)
                        if not(_la==MCParser.ADD or _la==MCParser.SUB):
                            self._errHandler.recoverInline(self)
                        else:
                            self._errHandler.reportMatch(self)
                            self.consume()
                        self.state = 209
                        self.expr(9)
                        pass

                    elif la_ == 3:
                        localctx = MCParser.ExprContext(self, _parentctx, _parentState)
                        self.pushNewRecursionContext(localctx, _startState, self.RULE_expr)
                        self.state = 210
                        if not self.precpred(self._ctx, 7):
                            from antlr4.error.Errors import FailedPredicateException
                            raise FailedPredicateException(self, "self.precpred(self._ctx, 7)")
                        self.state = 211
                        _la = self._input.LA(1)
                        if not((((_la) & ~0x3f) == 0 and ((1 << _la) & ((1 << MCParser.LT) | (1 << MCParser.GT) | (1 << MCParser.LTEQ) | (1 << MCParser.GTEQ))) != 0)):
                            self._errHandler.recoverInline(self)
                        else:
                            self._errHandler.reportMatch(self)
                            self.consume()
                        self.state = 212
                        self.expr(8)
                        pass

                    elif la_ == 4:
                        localctx = MCParser.ExprContext(self, _parentctx, _parentState)
                        self.pushNewRecursionContext(localctx, _startState, self.RULE_expr)
                        self.state = 213
                        if not self.precpred(self._ctx, 6):
                            from antlr4.error.Errors import FailedPredicateException
                            raise FailedPredicateException(self, "self.precpred(self._ctx, 6)")
                        self.state = 214
                        _la = self._input.LA(1)
                        if not(_la==MCParser.EQ or _la==MCParser.NEQ):
                            self._errHandler.recoverInline(self)
                        else:
                            self._errHandler.reportMatch(self)
                            self.consume()
                        self.state = 215
                        self.expr(7)
                        pass

                    elif la_ == 5:
                        localctx = MCParser.ExprContext(self, _parentctx, _parentState)
                        self.pushNewRecursionContext(localctx, _startState, self.RULE_expr)
                        self.state = 216
                        if not self.precpred(self._ctx, 5):
                            from antlr4.error.Errors import FailedPredicateException
                            raise FailedPredicateException(self, "self.precpred(self._ctx, 5)")
                        self.state = 217
                        _la = self._input.LA(1)
                        if not(_la==MCParser.OR or _la==MCParser.AND):
                            self._errHandler.recoverInline(self)
                        else:
                            self._errHandler.reportMatch(self)
                            self.consume()
                        self.state = 218
                        self.expr(6)
                        pass

                    elif la_ == 6:
                        localctx = MCParser.ExprContext(self, _parentctx, _parentState)
                        self.pushNewRecursionContext(localctx, _startState, self.RULE_expr)
                        self.state = 219
                        if not self.precpred(self._ctx, 1):
                            from antlr4.error.Errors import FailedPredicateException
                            raise FailedPredicateException(self, "self.precpred(self._ctx, 1)")
                        self.state = 220
                        self.match(MCParser.ASIGN)
                        self.state = 221
                        self.expr(1)
                        pass

                    elif la_ == 7:
                        localctx = MCParser.ExprContext(self, _parentctx, _parentState)
                        self.pushNewRecursionContext(localctx, _startState, self.RULE_expr)
                        self.state = 222
                        if not self.precpred(self._ctx, 11):
                            from antlr4.error.Errors import FailedPredicateException
                            raise FailedPredicateException(self, "self.precpred(self._ctx, 11)")
                        self.state = 223
                        self.match(MCParser.LS)
                        self.state = 224
                        self.expr(0)
                        self.state = 225
                        self.match(MCParser.RS)
                        pass

             
                self.state = 231
                self._errHandler.sync(self)
                _alt = self._interp.adaptivePredict(self._input,19,self._ctx)

        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.unrollRecursionContexts(_parentctx)
        return localctx


    class ParExprContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def LB(self):
            return self.getToken(MCParser.LB, 0)

        def expr(self):
            return self.getTypedRuleContext(MCParser.ExprContext,0)


        def RB(self):
            return self.getToken(MCParser.RB, 0)

        def getRuleIndex(self):
            return MCParser.RULE_parExpr

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitParExpr" ):
                return visitor.visitParExpr(self)
            else:
                return visitor.visitChildren(self)




    def parExpr(self):

        localctx = MCParser.ParExprContext(self, self._ctx, self.state)
        self.enterRule(localctx, 42, self.RULE_parExpr)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 232
            self.match(MCParser.LB)
            self.state = 233
            self.expr(0)
            self.state = 234
            self.match(MCParser.RB)
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class FunCallContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def ID(self):
            return self.getToken(MCParser.ID, 0)

        def LB(self):
            return self.getToken(MCParser.LB, 0)

        def funCallParams(self):
            return self.getTypedRuleContext(MCParser.FunCallParamsContext,0)


        def RB(self):
            return self.getToken(MCParser.RB, 0)

        def getRuleIndex(self):
            return MCParser.RULE_funCall

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitFunCall" ):
                return visitor.visitFunCall(self)
            else:
                return visitor.visitChildren(self)




    def funCall(self):

        localctx = MCParser.FunCallContext(self, self._ctx, self.state)
        self.enterRule(localctx, 44, self.RULE_funCall)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 236
            self.match(MCParser.ID)
            self.state = 237
            self.match(MCParser.LB)
            self.state = 238
            self.funCallParams()
            self.state = 239
            self.match(MCParser.RB)
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class FunCallParamsContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def expr(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(MCParser.ExprContext)
            else:
                return self.getTypedRuleContext(MCParser.ExprContext,i)


        def COMMA(self, i:int=None):
            if i is None:
                return self.getTokens(MCParser.COMMA)
            else:
                return self.getToken(MCParser.COMMA, i)

        def getRuleIndex(self):
            return MCParser.RULE_funCallParams

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitFunCallParams" ):
                return visitor.visitFunCallParams(self)
            else:
                return visitor.visitChildren(self)




    def funCallParams(self):

        localctx = MCParser.FunCallParamsContext(self, self._ctx, self.state)
        self.enterRule(localctx, 46, self.RULE_funCallParams)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 249
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            if (((_la) & ~0x3f) == 0 and ((1 << _la) & ((1 << MCParser.SUB) | (1 << MCParser.NOT) | (1 << MCParser.LB) | (1 << MCParser.IntLit) | (1 << MCParser.FloatLit) | (1 << MCParser.BoolLit) | (1 << MCParser.StrLit) | (1 << MCParser.ID))) != 0):
                self.state = 241
                self.expr(0)
                self.state = 246
                self._errHandler.sync(self)
                _la = self._input.LA(1)
                while _la==MCParser.COMMA:
                    self.state = 242
                    self.match(MCParser.COMMA)
                    self.state = 243
                    self.expr(0)
                    self.state = 248
                    self._errHandler.sync(self)
                    _la = self._input.LA(1)



        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx



    def sempred(self, localctx:RuleContext, ruleIndex:int, predIndex:int):
        if self._predicates == None:
            self._predicates = dict()
        self._predicates[20] = self.expr_sempred
        pred = self._predicates.get(ruleIndex, None)
        if pred is None:
            raise Exception("No predicate with index:" + str(ruleIndex))
        else:
            return pred(localctx, predIndex)

    def expr_sempred(self, localctx:ExprContext, predIndex:int):
            if predIndex == 0:
                return self.precpred(self._ctx, 9)
         

            if predIndex == 1:
                return self.precpred(self._ctx, 8)
         

            if predIndex == 2:
                return self.precpred(self._ctx, 7)
         

            if predIndex == 3:
                return self.precpred(self._ctx, 6)
         

            if predIndex == 4:
                return self.precpred(self._ctx, 5)
         

            if predIndex == 5:
                return self.precpred(self._ctx, 1)
         

            if predIndex == 6:
                return self.precpred(self._ctx, 11)
         




