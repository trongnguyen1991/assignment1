import unittest
# from TestUtils import TestLexer
from TestUtils import TestLexer

class LexerSuite(unittest.TestCase):
      
    def test_lower_identifier(self):
        """test identifiers"""
        self.assertTrue(TestLexer.checkLexeme("abc","abc,<EOF>",101))
    def test_lower_upper_id(self):
        self.assertTrue(TestLexer.checkLexeme("aCBbdc","aCBbdc,<EOF>",102))
    def test_complex_single_id(self):
        self.assertTrue(TestLexer.checkLexeme("_a23bc","_a23bc,<EOF>",103))
    def test_multiple_id(self):
        self.assertTrue(TestLexer.checkLexeme("_a 23bc","_a,23,bc,<EOF>",104))
    def test_wrong_token(self):
        self.assertTrue(TestLexer.checkLexeme("aA?sVN","aA,Error Token ?",105))
    def test_integer(self):
        self.assertTrue(TestLexer.checkLexeme("123a123","123,a123,<EOF>",106))
    def test_integer_space(self):
        self.assertTrue(TestLexer.checkLexeme("123 a123","123,a123,<EOF>",107))
    def test_if_statement(self):
        self.assertTrue(TestLexer.checkLexeme("if(a<b){a=a+1;}","if,(,a,<,b,),{,a,=,a,+,1,;,},<EOF>",108))
    def test_flus_id(self):
        self.assertTrue(TestLexer.checkLexeme("a + b","a,+,b,<EOF>",109))
    def test_mul_id(self):
        self.assertTrue(TestLexer.checkLexeme("a * b","a,*,b,<EOF>",110))
    def test_div_id(self):
        self.assertTrue(TestLexer.checkLexeme("a / b","a,/,b,<EOF>",111))
    def test_mod_id(self):
        self.assertTrue(TestLexer.checkLexeme("a % b","a,%,b,<EOF>",112))
    def test_assign_id(self):
        self.assertTrue(TestLexer.checkLexeme("a = b","a,=,b,<EOF>",113))
    def test_and_operator(self):
        self.assertTrue(TestLexer.checkLexeme("a && b","a,&&,b,<EOF>",114))
    def test_or_operator(self):
        self.assertTrue(TestLexer.checkLexeme("a || b","a,||,b,<EOF>",115))
    def test_complex_expr(self):
        self.assertTrue(TestLexer.checkLexeme("a + b && c - d <= x *y","a,+,b,&&,c,-,d,<=,x,*,y,<EOF>",116))
    def test_complex_expr_statement(self):
        self.assertTrue(TestLexer.checkLexeme("x = a % b - c / d","x,=,a,%,b,-,c,/,d,<EOF>",117))
    def test_not_and_equal_stm(self):
        self.assertTrue(TestLexer.checkLexeme("!a == b","!,a,==,b,<EOF>",118))
    def test_greater_than(self):
        self.assertTrue(TestLexer.checkLexeme("a >= b","a,>=,b,<EOF>",119))
    def test_less_than(self):
        self.assertTrue(TestLexer.checkLexeme("a <= b","a,<=,b,<EOF>",120))
    def test_greater_than_and_less_than(self):
        self.assertTrue(TestLexer.checkLexeme("a >= b || b <= c","a,>=,b,||,b,<=,c,<EOF>",121))
    def test_not_qual(self):
        self.assertTrue(TestLexer.checkLexeme("a != b","a,!=,b,<EOF>",122))
    def test_not_equal_and_grater_than(self):
        self.assertTrue(TestLexer.checkLexeme("a != b && a >= b","a,!=,b,&&,a,>=,b,<EOF>",123))
    def test_declare_boolean_type(self):
        self.assertTrue(TestLexer.checkLexeme("boolean a = true;","boolean,a,=,true,;,<EOF>",124))
    def test_boolean_value_and_assignment(self):
        self.assertTrue(TestLexer.checkLexeme("if true = false a = b; a= c;","if,true,=,false,a,=,b,;,a,=,c,;,<EOF>",125))
    def test_declare_float_type(self):
        self.assertTrue(TestLexer.checkLexeme("float a = 2.3;","float,a,=,2.3,;,<EOF>",126))