import unittest
from TestUtils import TestParser

class ParserSuite(unittest.TestCase):
    def test_simple_program(self):
        """Simple program: int main() {} """
        input = """int main() {int a;}"""
        expect = "successful"
        self.assertTrue(TestParser.checkParser(input,expect,201))

    def test_more_complex_program(self):
        """More complex program"""
        input = """int main () {
            putIntLn(4);
        }"""
        expect = "successful"
        self.assertTrue(TestParser.checkParser(input,expect,202))
    
    def test_wrong_miss_close(self):
        """Miss ) int main( {}"""
        input = """int main( {}"""
        expect = "Error on line 1 col 10: {"
        self.assertTrue(TestParser.checkParser(input,expect,203))

    def test_main_func_with_loop_statement(self):
        input = """int main(){
          int id;
          string name;
          id = 10;
          name = "Nguyen Van A";
          for (i = 0; i < 100; i=i+1){
            callAnotherFunction(id, name);
          }
        }"""
        expect = "sucessful"
        self.assertTrue(TestParser.checkParser(input,expect,204))

    def test_complex_function_in_mc(self):
        input = """
            int i ;
            int f ( ) {
                return 200;
            }
            void main ( ) {
                int main ;
                main = f ( ) ;
                putIntLn ( i ) ;
                {
                    int i ;
                    int main ;
                    int f ;
                    main = f = i = 100;
                    putIntLn ( i ) ;
                    putIntLn ( main ) ;
                    putIntLn ( f ) ;
                }
                putIntLn ( main ) ;
            }
        """
        expect = "sucessful"
        self.assertTrue(TestParser.checkParser(input,expect,205))

    def test_do_while_statement(self):
        input = """
            int n;
            int sum;
            int i;
            int main(){
              n = 10;
              sum = 0;
              i = 1;
              do{
                  sum = sum + i;
                  i = i + 1;
              } while (i <= n);
            }
        """
        expect = "sucessful"
        self.assertTrue(TestParser.checkParser(input,expect,206))

    def test_complex_with_almost_of_supported_keywords(self):
        input = """
            int main() {
              do {
                  boolean isPrime;
                  int j;
                  isPrime = anyFunction();
                  j = isPrime;
                  do{
                      if (j != 1 && num % j == 0) {
                          isPrime = false;
                          break;
                      } else {
                          j = j + 1;
                      }
                  }while (j < num / 2);
                  if (isPrime) {
                      if (count < desiredCount - 1)
                         count = count + 1;
                  }
                  num = num + 1;
              } while (count < desiredCount);
            }
        """
        expect = "sucessful"
        self.assertTrue(TestParser.checkParser(input,expect,207))

    def test_array_declare(self):
        input = """
            int main(){
              int ids[10];
            }
        """
        expect = "sucessful"
        self.assertTrue(TestParser.checkParser(input,expect,208))

    def test_wrong_array_declare(self):
        input = """int main(){
                      int ids[9.1];
                    }
        """
        expect = "Error on line 2 col 31: 9.1"
        self.assertTrue(TestParser.checkParser(input,expect,209))

    def test_nested_if_else(self):
        input = """void test(boolean bs[], int numbers[], float a){
         if (a < 100)
           if (b)
             return bs[0];
           else
             return bs[1];
        
         return 0;
        }
        """
        expect = "sucessful"
        self.assertTrue(TestParser.checkParser(input,expect,210))

    def test_array_assignment(self):
        input = """void main(){
          int a[2];
          a[0] = 1;
        }
        """
        expect = "sucessful"
        self.assertTrue(TestParser.checkParser(input,expect,211))

    def test_multi_variable(self):
        input = """int main(){
          int a,b,c,d[],e[3];
        }
        """
        expect = "sucessful"
        self.assertTrue(TestParser.checkParser(input,expect,212))

    def test_var_declare(self):
        input = """int main(){
            int a;
            boolean b;
            float c[2];
            string d;
        }
        """
        expect = "sucessful"
        self.assertTrue(TestParser.checkParser(input,expect,213))