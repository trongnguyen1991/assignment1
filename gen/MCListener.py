# Generated from /Users/trongnguyen/Master/initial/src/main/mc/parser/MC.g4 by ANTLR 4.7.2
from antlr4 import *
if __name__ is not None and "." in __name__:
    from .MCParser import MCParser
else:
    from MCParser import MCParser

# This class defines a complete listener for a parse tree produced by MCParser.
class MCListener(ParseTreeListener):

    # Enter a parse tree produced by MCParser#program.
    def enterProgram(self, ctx:MCParser.ProgramContext):
        pass

    # Exit a parse tree produced by MCParser#program.
    def exitProgram(self, ctx:MCParser.ProgramContext):
        pass


    # Enter a parse tree produced by MCParser#decl.
    def enterDecl(self, ctx:MCParser.DeclContext):
        pass

    # Exit a parse tree produced by MCParser#decl.
    def exitDecl(self, ctx:MCParser.DeclContext):
        pass


    # Enter a parse tree produced by MCParser#funcDecl.
    def enterFuncDecl(self, ctx:MCParser.FuncDeclContext):
        pass

    # Exit a parse tree produced by MCParser#funcDecl.
    def exitFuncDecl(self, ctx:MCParser.FuncDeclContext):
        pass


    # Enter a parse tree produced by MCParser#funType.
    def enterFunType(self, ctx:MCParser.FunTypeContext):
        pass

    # Exit a parse tree produced by MCParser#funType.
    def exitFunType(self, ctx:MCParser.FunTypeContext):
        pass


    # Enter a parse tree produced by MCParser#funReturnType.
    def enterFunReturnType(self, ctx:MCParser.FunReturnTypeContext):
        pass

    # Exit a parse tree produced by MCParser#funReturnType.
    def exitFunReturnType(self, ctx:MCParser.FunReturnTypeContext):
        pass


    # Enter a parse tree produced by MCParser#paramList.
    def enterParamList(self, ctx:MCParser.ParamListContext):
        pass

    # Exit a parse tree produced by MCParser#paramList.
    def exitParamList(self, ctx:MCParser.ParamListContext):
        pass


    # Enter a parse tree produced by MCParser#funcParName.
    def enterFuncParName(self, ctx:MCParser.FuncParNameContext):
        pass

    # Exit a parse tree produced by MCParser#funcParName.
    def exitFuncParName(self, ctx:MCParser.FuncParNameContext):
        pass


    # Enter a parse tree produced by MCParser#varType.
    def enterVarType(self, ctx:MCParser.VarTypeContext):
        pass

    # Exit a parse tree produced by MCParser#varType.
    def exitVarType(self, ctx:MCParser.VarTypeContext):
        pass


    # Enter a parse tree produced by MCParser#block.
    def enterBlock(self, ctx:MCParser.BlockContext):
        pass

    # Exit a parse tree produced by MCParser#block.
    def exitBlock(self, ctx:MCParser.BlockContext):
        pass


    # Enter a parse tree produced by MCParser#varDecl.
    def enterVarDecl(self, ctx:MCParser.VarDeclContext):
        pass

    # Exit a parse tree produced by MCParser#varDecl.
    def exitVarDecl(self, ctx:MCParser.VarDeclContext):
        pass


    # Enter a parse tree produced by MCParser#varName.
    def enterVarName(self, ctx:MCParser.VarNameContext):
        pass

    # Exit a parse tree produced by MCParser#varName.
    def exitVarName(self, ctx:MCParser.VarNameContext):
        pass


    # Enter a parse tree produced by MCParser#stmt.
    def enterStmt(self, ctx:MCParser.StmtContext):
        pass

    # Exit a parse tree produced by MCParser#stmt.
    def exitStmt(self, ctx:MCParser.StmtContext):
        pass


    # Enter a parse tree produced by MCParser#nonIfStmt.
    def enterNonIfStmt(self, ctx:MCParser.NonIfStmtContext):
        pass

    # Exit a parse tree produced by MCParser#nonIfStmt.
    def exitNonIfStmt(self, ctx:MCParser.NonIfStmtContext):
        pass


    # Enter a parse tree produced by MCParser#ifelse.
    def enterIfelse(self, ctx:MCParser.IfelseContext):
        pass

    # Exit a parse tree produced by MCParser#ifelse.
    def exitIfelse(self, ctx:MCParser.IfelseContext):
        pass


    # Enter a parse tree produced by MCParser#match.
    def enterMatch(self, ctx:MCParser.MatchContext):
        pass

    # Exit a parse tree produced by MCParser#match.
    def exitMatch(self, ctx:MCParser.MatchContext):
        pass


    # Enter a parse tree produced by MCParser#unMatch.
    def enterUnMatch(self, ctx:MCParser.UnMatchContext):
        pass

    # Exit a parse tree produced by MCParser#unMatch.
    def exitUnMatch(self, ctx:MCParser.UnMatchContext):
        pass


    # Enter a parse tree produced by MCParser#dowhile.
    def enterDowhile(self, ctx:MCParser.DowhileContext):
        pass

    # Exit a parse tree produced by MCParser#dowhile.
    def exitDowhile(self, ctx:MCParser.DowhileContext):
        pass


    # Enter a parse tree produced by MCParser#forloop.
    def enterForloop(self, ctx:MCParser.ForloopContext):
        pass

    # Exit a parse tree produced by MCParser#forloop.
    def exitForloop(self, ctx:MCParser.ForloopContext):
        pass


    # Enter a parse tree produced by MCParser#dataTypeLit.
    def enterDataTypeLit(self, ctx:MCParser.DataTypeLitContext):
        pass

    # Exit a parse tree produced by MCParser#dataTypeLit.
    def exitDataTypeLit(self, ctx:MCParser.DataTypeLitContext):
        pass


    # Enter a parse tree produced by MCParser#returnStmt.
    def enterReturnStmt(self, ctx:MCParser.ReturnStmtContext):
        pass

    # Exit a parse tree produced by MCParser#returnStmt.
    def exitReturnStmt(self, ctx:MCParser.ReturnStmtContext):
        pass


    # Enter a parse tree produced by MCParser#expr.
    def enterExpr(self, ctx:MCParser.ExprContext):
        pass

    # Exit a parse tree produced by MCParser#expr.
    def exitExpr(self, ctx:MCParser.ExprContext):
        pass


    # Enter a parse tree produced by MCParser#parExpr.
    def enterParExpr(self, ctx:MCParser.ParExprContext):
        pass

    # Exit a parse tree produced by MCParser#parExpr.
    def exitParExpr(self, ctx:MCParser.ParExprContext):
        pass


    # Enter a parse tree produced by MCParser#funCall.
    def enterFunCall(self, ctx:MCParser.FunCallContext):
        pass

    # Exit a parse tree produced by MCParser#funCall.
    def exitFunCall(self, ctx:MCParser.FunCallContext):
        pass


    # Enter a parse tree produced by MCParser#funCallParams.
    def enterFunCallParams(self, ctx:MCParser.FunCallParamsContext):
        pass

    # Exit a parse tree produced by MCParser#funCallParams.
    def exitFunCallParams(self, ctx:MCParser.FunCallParamsContext):
        pass


