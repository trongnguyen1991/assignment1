# Generated from /Users/trongnguyen/Master/initial/src/main/mc/parser/MC.g4 by ANTLR 4.7.2
from antlr4 import *
from io import StringIO
from typing.io import TextIO
import sys


from lexererr import *



def serializedATN():
    with StringIO() as buf:
        buf.write("\3\u608b\ua72a\u8133\ub9ed\u417c\u3be7\u7786\u5964\2\61")
        buf.write("\u018a\b\1\4\2\t\2\4\3\t\3\4\4\t\4\4\5\t\5\4\6\t\6\4\7")
        buf.write("\t\7\4\b\t\b\4\t\t\t\4\n\t\n\4\13\t\13\4\f\t\f\4\r\t\r")
        buf.write("\4\16\t\16\4\17\t\17\4\20\t\20\4\21\t\21\4\22\t\22\4\23")
        buf.write("\t\23\4\24\t\24\4\25\t\25\4\26\t\26\4\27\t\27\4\30\t\30")
        buf.write("\4\31\t\31\4\32\t\32\4\33\t\33\4\34\t\34\4\35\t\35\4\36")
        buf.write("\t\36\4\37\t\37\4 \t \4!\t!\4\"\t\"\4#\t#\4$\t$\4%\t%")
        buf.write("\4&\t&\4\'\t\'\4(\t(\4)\t)\4*\t*\4+\t+\4,\t,\4-\t-\4.")
        buf.write("\t.\4/\t/\4\60\t\60\4\61\t\61\4\62\t\62\4\63\t\63\3\2")
        buf.write("\3\2\3\3\3\3\3\4\3\4\3\4\3\5\3\5\3\5\3\5\3\5\3\5\3\6\3")
        buf.write("\6\3\6\3\6\3\6\3\6\3\6\3\6\3\6\3\7\3\7\3\7\3\7\3\7\3\b")
        buf.write("\3\b\3\b\3\b\3\t\3\t\3\t\3\n\3\n\3\n\3\n\3\n\3\n\3\n\3")
        buf.write("\13\3\13\3\13\3\13\3\13\3\f\3\f\3\f\3\r\3\r\3\r\3\r\3")
        buf.write("\r\3\r\3\16\3\16\3\16\3\16\3\16\3\16\3\16\3\16\3\17\3")
        buf.write("\17\3\17\3\17\3\17\3\17\3\20\3\20\3\20\3\20\3\21\3\21")
        buf.write("\3\21\3\21\3\21\3\21\3\21\3\22\3\22\3\23\3\23\3\24\3\24")
        buf.write("\3\25\3\25\3\26\3\26\3\27\3\27\3\30\3\30\3\30\3\31\3\31")
        buf.write("\3\31\3\32\3\32\3\32\3\33\3\33\3\33\3\34\3\34\3\35\3\35")
        buf.write("\3\36\3\36\3\36\3\37\3\37\3\37\3 \3 \3!\3!\3\"\3\"\3#")
        buf.write("\3#\3$\3$\3%\3%\3&\3&\3\'\3\'\3(\3(\3)\6)\u00ed\n)\r)")
        buf.write("\16)\u00ee\3*\6*\u00f2\n*\r*\16*\u00f3\3*\3*\7*\u00f8")
        buf.write("\n*\f*\16*\u00fb\13*\3*\7*\u00fe\n*\f*\16*\u0101\13*\3")
        buf.write("*\3*\6*\u0105\n*\r*\16*\u0106\3*\6*\u010a\n*\r*\16*\u010b")
        buf.write("\3*\3*\5*\u0110\n*\3*\6*\u0113\n*\r*\16*\u0114\3*\7*\u0118")
        buf.write("\n*\f*\16*\u011b\13*\3*\3*\6*\u011f\n*\r*\16*\u0120\3")
        buf.write("*\3*\5*\u0125\n*\3*\6*\u0128\n*\r*\16*\u0129\5*\u012c")
        buf.write("\n*\3+\3+\3+\3+\3+\3+\3+\3+\3+\5+\u0137\n+\3,\3,\3,\7")
        buf.write(",\u013c\n,\f,\16,\u013f\13,\3,\3,\3,\3-\3-\3-\3-\7-\u0148")
        buf.write("\n-\f-\16-\u014b\13-\3-\3-\3-\3-\3-\3.\3.\3.\3.\7.\u0156")
        buf.write("\n.\f.\16.\u0159\13.\3.\3.\3/\6/\u015e\n/\r/\16/\u015f")
        buf.write("\3/\3/\3\60\3\60\5\60\u0166\n\60\3\60\3\60\3\60\7\60\u016b")
        buf.write("\n\60\f\60\16\60\u016e\13\60\3\61\3\61\7\61\u0172\n\61")
        buf.write("\f\61\16\61\u0175\13\61\3\61\3\61\7\61\u0179\n\61\f\61")
        buf.write("\16\61\u017c\13\61\3\61\3\61\3\62\3\62\7\62\u0182\n\62")
        buf.write("\f\62\16\62\u0185\13\62\3\62\3\62\3\63\3\63\3\u0149\2")
        buf.write("\64\3\2\5\2\7\2\t\3\13\4\r\5\17\6\21\7\23\b\25\t\27\n")
        buf.write("\31\13\33\f\35\r\37\16!\17#\20%\21\'\22)\23+\24-\25/\26")
        buf.write("\61\27\63\30\65\31\67\329\33;\34=\35?\36A\37C E!G\"I#")
        buf.write("K$M%O&Q\'S(U)W*Y+[,]-_.a/c\60e\61\3\2\f\3\2\62;\4\2C\\")
        buf.write("c|\n\2$$))^^ddhhppttvv\4\2GGgg\4\2--//\6\2\f\f\17\17$")
        buf.write("$^^\4\2\f\f\17\17\5\2\n\f\16\17\"\"\5\2\f\f\17\17$$\3")
        buf.write("\2$$\2\u01a2\2\t\3\2\2\2\2\13\3\2\2\2\2\r\3\2\2\2\2\17")
        buf.write("\3\2\2\2\2\21\3\2\2\2\2\23\3\2\2\2\2\25\3\2\2\2\2\27\3")
        buf.write("\2\2\2\2\31\3\2\2\2\2\33\3\2\2\2\2\35\3\2\2\2\2\37\3\2")
        buf.write("\2\2\2!\3\2\2\2\2#\3\2\2\2\2%\3\2\2\2\2\'\3\2\2\2\2)\3")
        buf.write("\2\2\2\2+\3\2\2\2\2-\3\2\2\2\2/\3\2\2\2\2\61\3\2\2\2\2")
        buf.write("\63\3\2\2\2\2\65\3\2\2\2\2\67\3\2\2\2\29\3\2\2\2\2;\3")
        buf.write("\2\2\2\2=\3\2\2\2\2?\3\2\2\2\2A\3\2\2\2\2C\3\2\2\2\2E")
        buf.write("\3\2\2\2\2G\3\2\2\2\2I\3\2\2\2\2K\3\2\2\2\2M\3\2\2\2\2")
        buf.write("O\3\2\2\2\2Q\3\2\2\2\2S\3\2\2\2\2U\3\2\2\2\2W\3\2\2\2")
        buf.write("\2Y\3\2\2\2\2[\3\2\2\2\2]\3\2\2\2\2_\3\2\2\2\2a\3\2\2")
        buf.write("\2\2c\3\2\2\2\2e\3\2\2\2\3g\3\2\2\2\5i\3\2\2\2\7k\3\2")
        buf.write("\2\2\tn\3\2\2\2\13t\3\2\2\2\r}\3\2\2\2\17\u0082\3\2\2")
        buf.write("\2\21\u0086\3\2\2\2\23\u0089\3\2\2\2\25\u0090\3\2\2\2")
        buf.write("\27\u0095\3\2\2\2\31\u0098\3\2\2\2\33\u009e\3\2\2\2\35")
        buf.write("\u00a6\3\2\2\2\37\u00ac\3\2\2\2!\u00b0\3\2\2\2#\u00b7")
        buf.write("\3\2\2\2%\u00b9\3\2\2\2\'\u00bb\3\2\2\2)\u00bd\3\2\2\2")
        buf.write("+\u00bf\3\2\2\2-\u00c1\3\2\2\2/\u00c3\3\2\2\2\61\u00c6")
        buf.write("\3\2\2\2\63\u00c9\3\2\2\2\65\u00cc\3\2\2\2\67\u00cf\3")
        buf.write("\2\2\29\u00d1\3\2\2\2;\u00d3\3\2\2\2=\u00d6\3\2\2\2?\u00d9")
        buf.write("\3\2\2\2A\u00db\3\2\2\2C\u00dd\3\2\2\2E\u00df\3\2\2\2")
        buf.write("G\u00e1\3\2\2\2I\u00e3\3\2\2\2K\u00e5\3\2\2\2M\u00e7\3")
        buf.write("\2\2\2O\u00e9\3\2\2\2Q\u00ec\3\2\2\2S\u012b\3\2\2\2U\u0136")
        buf.write("\3\2\2\2W\u0138\3\2\2\2Y\u0143\3\2\2\2[\u0151\3\2\2\2")
        buf.write("]\u015d\3\2\2\2_\u0165\3\2\2\2a\u016f\3\2\2\2c\u017f\3")
        buf.write("\2\2\2e\u0188\3\2\2\2gh\t\2\2\2h\4\3\2\2\2ij\t\3\2\2j")
        buf.write("\6\3\2\2\2kl\7^\2\2lm\t\4\2\2m\b\3\2\2\2no\7d\2\2op\7")
        buf.write("t\2\2pq\7g\2\2qr\7c\2\2rs\7m\2\2s\n\3\2\2\2tu\7e\2\2u")
        buf.write("v\7q\2\2vw\7p\2\2wx\7v\2\2xy\7k\2\2yz\7p\2\2z{\7w\2\2")
        buf.write("{|\7g\2\2|\f\3\2\2\2}~\7g\2\2~\177\7n\2\2\177\u0080\7")
        buf.write("u\2\2\u0080\u0081\7g\2\2\u0081\16\3\2\2\2\u0082\u0083")
        buf.write("\7h\2\2\u0083\u0084\7q\2\2\u0084\u0085\7t\2\2\u0085\20")
        buf.write("\3\2\2\2\u0086\u0087\7k\2\2\u0087\u0088\7h\2\2\u0088\22")
        buf.write("\3\2\2\2\u0089\u008a\7t\2\2\u008a\u008b\7g\2\2\u008b\u008c")
        buf.write("\7v\2\2\u008c\u008d\7w\2\2\u008d\u008e\7t\2\2\u008e\u008f")
        buf.write("\7p\2\2\u008f\24\3\2\2\2\u0090\u0091\7x\2\2\u0091\u0092")
        buf.write("\7q\2\2\u0092\u0093\7k\2\2\u0093\u0094\7f\2\2\u0094\26")
        buf.write("\3\2\2\2\u0095\u0096\7f\2\2\u0096\u0097\7q\2\2\u0097\30")
        buf.write("\3\2\2\2\u0098\u0099\7y\2\2\u0099\u009a\7j\2\2\u009a\u009b")
        buf.write("\7k\2\2\u009b\u009c\7n\2\2\u009c\u009d\7g\2\2\u009d\32")
        buf.write("\3\2\2\2\u009e\u009f\7d\2\2\u009f\u00a0\7q\2\2\u00a0\u00a1")
        buf.write("\7q\2\2\u00a1\u00a2\7n\2\2\u00a2\u00a3\7g\2\2\u00a3\u00a4")
        buf.write("\7c\2\2\u00a4\u00a5\7p\2\2\u00a5\34\3\2\2\2\u00a6\u00a7")
        buf.write("\7h\2\2\u00a7\u00a8\7n\2\2\u00a8\u00a9\7q\2\2\u00a9\u00aa")
        buf.write("\7c\2\2\u00aa\u00ab\7v\2\2\u00ab\36\3\2\2\2\u00ac\u00ad")
        buf.write("\7k\2\2\u00ad\u00ae\7p\2\2\u00ae\u00af\7v\2\2\u00af \3")
        buf.write("\2\2\2\u00b0\u00b1\7u\2\2\u00b1\u00b2\7v\2\2\u00b2\u00b3")
        buf.write("\7t\2\2\u00b3\u00b4\7k\2\2\u00b4\u00b5\7p\2\2\u00b5\u00b6")
        buf.write("\7i\2\2\u00b6\"\3\2\2\2\u00b7\u00b8\7-\2\2\u00b8$\3\2")
        buf.write("\2\2\u00b9\u00ba\7/\2\2\u00ba&\3\2\2\2\u00bb\u00bc\7,")
        buf.write("\2\2\u00bc(\3\2\2\2\u00bd\u00be\7\61\2\2\u00be*\3\2\2")
        buf.write("\2\u00bf\u00c0\7#\2\2\u00c0,\3\2\2\2\u00c1\u00c2\7\'\2")
        buf.write("\2\u00c2.\3\2\2\2\u00c3\u00c4\7~\2\2\u00c4\u00c5\7~\2")
        buf.write("\2\u00c5\60\3\2\2\2\u00c6\u00c7\7(\2\2\u00c7\u00c8\7(")
        buf.write("\2\2\u00c8\62\3\2\2\2\u00c9\u00ca\7?\2\2\u00ca\u00cb\7")
        buf.write("?\2\2\u00cb\64\3\2\2\2\u00cc\u00cd\7#\2\2\u00cd\u00ce")
        buf.write("\7?\2\2\u00ce\66\3\2\2\2\u00cf\u00d0\7>\2\2\u00d08\3\2")
        buf.write("\2\2\u00d1\u00d2\7@\2\2\u00d2:\3\2\2\2\u00d3\u00d4\7>")
        buf.write("\2\2\u00d4\u00d5\7?\2\2\u00d5<\3\2\2\2\u00d6\u00d7\7@")
        buf.write("\2\2\u00d7\u00d8\7?\2\2\u00d8>\3\2\2\2\u00d9\u00da\7?")
        buf.write("\2\2\u00da@\3\2\2\2\u00db\u00dc\7]\2\2\u00dcB\3\2\2\2")
        buf.write("\u00dd\u00de\7_\2\2\u00deD\3\2\2\2\u00df\u00e0\7}\2\2")
        buf.write("\u00e0F\3\2\2\2\u00e1\u00e2\7\177\2\2\u00e2H\3\2\2\2\u00e3")
        buf.write("\u00e4\7*\2\2\u00e4J\3\2\2\2\u00e5\u00e6\7+\2\2\u00e6")
        buf.write("L\3\2\2\2\u00e7\u00e8\7=\2\2\u00e8N\3\2\2\2\u00e9\u00ea")
        buf.write("\7.\2\2\u00eaP\3\2\2\2\u00eb\u00ed\5\3\2\2\u00ec\u00eb")
        buf.write("\3\2\2\2\u00ed\u00ee\3\2\2\2\u00ee\u00ec\3\2\2\2\u00ee")
        buf.write("\u00ef\3\2\2\2\u00efR\3\2\2\2\u00f0\u00f2\5\3\2\2\u00f1")
        buf.write("\u00f0\3\2\2\2\u00f2\u00f3\3\2\2\2\u00f3\u00f1\3\2\2\2")
        buf.write("\u00f3\u00f4\3\2\2\2\u00f4\u00f5\3\2\2\2\u00f5\u00f9\7")
        buf.write("\60\2\2\u00f6\u00f8\5\3\2\2\u00f7\u00f6\3\2\2\2\u00f8")
        buf.write("\u00fb\3\2\2\2\u00f9\u00f7\3\2\2\2\u00f9\u00fa\3\2\2\2")
        buf.write("\u00fa\u012c\3\2\2\2\u00fb\u00f9\3\2\2\2\u00fc\u00fe\5")
        buf.write("\3\2\2\u00fd\u00fc\3\2\2\2\u00fe\u0101\3\2\2\2\u00ff\u00fd")
        buf.write("\3\2\2\2\u00ff\u0100\3\2\2\2\u0100\u0102\3\2\2\2\u0101")
        buf.write("\u00ff\3\2\2\2\u0102\u0104\7\60\2\2\u0103\u0105\5\3\2")
        buf.write("\2\u0104\u0103\3\2\2\2\u0105\u0106\3\2\2\2\u0106\u0104")
        buf.write("\3\2\2\2\u0106\u0107\3\2\2\2\u0107\u012c\3\2\2\2\u0108")
        buf.write("\u010a\5\3\2\2\u0109\u0108\3\2\2\2\u010a\u010b\3\2\2\2")
        buf.write("\u010b\u0109\3\2\2\2\u010b\u010c\3\2\2\2\u010c\u010d\3")
        buf.write("\2\2\2\u010d\u010f\t\5\2\2\u010e\u0110\t\6\2\2\u010f\u010e")
        buf.write("\3\2\2\2\u010f\u0110\3\2\2\2\u0110\u0112\3\2\2\2\u0111")
        buf.write("\u0113\5\3\2\2\u0112\u0111\3\2\2\2\u0113\u0114\3\2\2\2")
        buf.write("\u0114\u0112\3\2\2\2\u0114\u0115\3\2\2\2\u0115\u012c\3")
        buf.write("\2\2\2\u0116\u0118\5\3\2\2\u0117\u0116\3\2\2\2\u0118\u011b")
        buf.write("\3\2\2\2\u0119\u0117\3\2\2\2\u0119\u011a\3\2\2\2\u011a")
        buf.write("\u011c\3\2\2\2\u011b\u0119\3\2\2\2\u011c\u011e\7\60\2")
        buf.write("\2\u011d\u011f\5\3\2\2\u011e\u011d\3\2\2\2\u011f\u0120")
        buf.write("\3\2\2\2\u0120\u011e\3\2\2\2\u0120\u0121\3\2\2\2\u0121")
        buf.write("\u0122\3\2\2\2\u0122\u0124\t\5\2\2\u0123\u0125\t\6\2\2")
        buf.write("\u0124\u0123\3\2\2\2\u0124\u0125\3\2\2\2\u0125\u0127\3")
        buf.write("\2\2\2\u0126\u0128\5\3\2\2\u0127\u0126\3\2\2\2\u0128\u0129")
        buf.write("\3\2\2\2\u0129\u0127\3\2\2\2\u0129\u012a\3\2\2\2\u012a")
        buf.write("\u012c\3\2\2\2\u012b\u00f1\3\2\2\2\u012b\u00ff\3\2\2\2")
        buf.write("\u012b\u0109\3\2\2\2\u012b\u0119\3\2\2\2\u012cT\3\2\2")
        buf.write("\2\u012d\u012e\7v\2\2\u012e\u012f\7t\2\2\u012f\u0130\7")
        buf.write("w\2\2\u0130\u0137\7g\2\2\u0131\u0132\7h\2\2\u0132\u0133")
        buf.write("\7c\2\2\u0133\u0134\7n\2\2\u0134\u0135\7u\2\2\u0135\u0137")
        buf.write("\7g\2\2\u0136\u012d\3\2\2\2\u0136\u0131\3\2\2\2\u0137")
        buf.write("V\3\2\2\2\u0138\u013d\7$\2\2\u0139\u013c\n\7\2\2\u013a")
        buf.write("\u013c\5\7\4\2\u013b\u0139\3\2\2\2\u013b\u013a\3\2\2\2")
        buf.write("\u013c\u013f\3\2\2\2\u013d\u013b\3\2\2\2\u013d\u013e\3")
        buf.write("\2\2\2\u013e\u0140\3\2\2\2\u013f\u013d\3\2\2\2\u0140\u0141")
        buf.write("\7$\2\2\u0141\u0142\b,\2\2\u0142X\3\2\2\2\u0143\u0144")
        buf.write("\7\61\2\2\u0144\u0145\7,\2\2\u0145\u0149\3\2\2\2\u0146")
        buf.write("\u0148\13\2\2\2\u0147\u0146\3\2\2\2\u0148\u014b\3\2\2")
        buf.write("\2\u0149\u014a\3\2\2\2\u0149\u0147\3\2\2\2\u014a\u014c")
        buf.write("\3\2\2\2\u014b\u0149\3\2\2\2\u014c\u014d\7,\2\2\u014d")
        buf.write("\u014e\7\61\2\2\u014e\u014f\3\2\2\2\u014f\u0150\b-\3\2")
        buf.write("\u0150Z\3\2\2\2\u0151\u0152\7\61\2\2\u0152\u0153\7\61")
        buf.write("\2\2\u0153\u0157\3\2\2\2\u0154\u0156\n\b\2\2\u0155\u0154")
        buf.write("\3\2\2\2\u0156\u0159\3\2\2\2\u0157\u0155\3\2\2\2\u0157")
        buf.write("\u0158\3\2\2\2\u0158\u015a\3\2\2\2\u0159\u0157\3\2\2\2")
        buf.write("\u015a\u015b\b.\3\2\u015b\\\3\2\2\2\u015c\u015e\t\t\2")
        buf.write("\2\u015d\u015c\3\2\2\2\u015e\u015f\3\2\2\2\u015f\u015d")
        buf.write("\3\2\2\2\u015f\u0160\3\2\2\2\u0160\u0161\3\2\2\2\u0161")
        buf.write("\u0162\b/\3\2\u0162^\3\2\2\2\u0163\u0166\7a\2\2\u0164")
        buf.write("\u0166\5\5\3\2\u0165\u0163\3\2\2\2\u0165\u0164\3\2\2\2")
        buf.write("\u0166\u016c\3\2\2\2\u0167\u016b\5\3\2\2\u0168\u016b\7")
        buf.write("a\2\2\u0169\u016b\5\5\3\2\u016a\u0167\3\2\2\2\u016a\u0168")
        buf.write("\3\2\2\2\u016a\u0169\3\2\2\2\u016b\u016e\3\2\2\2\u016c")
        buf.write("\u016a\3\2\2\2\u016c\u016d\3\2\2\2\u016d`\3\2\2\2\u016e")
        buf.write("\u016c\3\2\2\2\u016f\u0173\7$\2\2\u0170\u0172\n\n\2\2")
        buf.write("\u0171\u0170\3\2\2\2\u0172\u0175\3\2\2\2\u0173\u0171\3")
        buf.write("\2\2\2\u0173\u0174\3\2\2\2\u0174\u0176\3\2\2\2\u0175\u0173")
        buf.write("\3\2\2\2\u0176\u017a\7^\2\2\u0177\u0179\n\4\2\2\u0178")
        buf.write("\u0177\3\2\2\2\u0179\u017c\3\2\2\2\u017a\u0178\3\2\2\2")
        buf.write("\u017a\u017b\3\2\2\2\u017b\u017d\3\2\2\2\u017c\u017a\3")
        buf.write("\2\2\2\u017d\u017e\7$\2\2\u017eb\3\2\2\2\u017f\u0183\7")
        buf.write("$\2\2\u0180\u0182\n\13\2\2\u0181\u0180\3\2\2\2\u0182\u0185")
        buf.write("\3\2\2\2\u0183\u0181\3\2\2\2\u0183\u0184\3\2\2\2\u0184")
        buf.write("\u0186\3\2\2\2\u0185\u0183\3\2\2\2\u0186\u0187\7\2\2\3")
        buf.write("\u0187d\3\2\2\2\u0188\u0189\13\2\2\2\u0189f\3\2\2\2\34")
        buf.write("\2\u00ee\u00f3\u00f9\u00ff\u0106\u010b\u010f\u0114\u0119")
        buf.write("\u0120\u0124\u0129\u012b\u0136\u013b\u013d\u0149\u0157")
        buf.write("\u015f\u0165\u016a\u016c\u0173\u017a\u0183\4\3,\2\b\2")
        buf.write("\2")
        return buf.getvalue()


class MCLexer(Lexer):

    atn = ATNDeserializer().deserialize(serializedATN())

    decisionsToDFA = [ DFA(ds, i) for i, ds in enumerate(atn.decisionToState) ]

    BREAK = 1
    CONTINUE = 2
    ELSE = 3
    FOR = 4
    IF = 5
    RETURN = 6
    VOID = 7
    DO = 8
    WHILE = 9
    BOOLEAN = 10
    FLOAT = 11
    INT = 12
    STRING = 13
    ADD = 14
    SUB = 15
    MUL = 16
    DIV = 17
    NOT = 18
    MOD = 19
    OR = 20
    AND = 21
    EQ = 22
    NEQ = 23
    LT = 24
    GT = 25
    LTEQ = 26
    GTEQ = 27
    ASIGN = 28
    LS = 29
    RS = 30
    LP = 31
    RP = 32
    LB = 33
    RB = 34
    SEMI = 35
    COMMA = 36
    IntLit = 37
    FloatLit = 38
    BoolLit = 39
    StrLit = 40
    COMMENT = 41
    LINE_COMMENT = 42
    WS = 43
    ID = 44
    ILLEGAL_ESCAPE = 45
    UNCLOSE_STRING = 46
    ERROR_CHAR = 47

    channelNames = [ u"DEFAULT_TOKEN_CHANNEL", u"HIDDEN" ]

    modeNames = [ "DEFAULT_MODE" ]

    literalNames = [ "<INVALID>",
            "'break'", "'continue'", "'else'", "'for'", "'if'", "'return'", 
            "'void'", "'do'", "'while'", "'boolean'", "'float'", "'int'", 
            "'string'", "'+'", "'-'", "'*'", "'/'", "'!'", "'%'", "'||'", 
            "'&&'", "'=='", "'!='", "'<'", "'>'", "'<='", "'>='", "'='", 
            "'['", "']'", "'{'", "'}'", "'('", "')'", "';'", "','" ]

    symbolicNames = [ "<INVALID>",
            "BREAK", "CONTINUE", "ELSE", "FOR", "IF", "RETURN", "VOID", 
            "DO", "WHILE", "BOOLEAN", "FLOAT", "INT", "STRING", "ADD", "SUB", 
            "MUL", "DIV", "NOT", "MOD", "OR", "AND", "EQ", "NEQ", "LT", 
            "GT", "LTEQ", "GTEQ", "ASIGN", "LS", "RS", "LP", "RP", "LB", 
            "RB", "SEMI", "COMMA", "IntLit", "FloatLit", "BoolLit", "StrLit", 
            "COMMENT", "LINE_COMMENT", "WS", "ID", "ILLEGAL_ESCAPE", "UNCLOSE_STRING", 
            "ERROR_CHAR" ]

    ruleNames = [ "DIGIT", "CHARACTER", "ESCAPE_SEQUENCE", "BREAK", "CONTINUE", 
                  "ELSE", "FOR", "IF", "RETURN", "VOID", "DO", "WHILE", 
                  "BOOLEAN", "FLOAT", "INT", "STRING", "ADD", "SUB", "MUL", 
                  "DIV", "NOT", "MOD", "OR", "AND", "EQ", "NEQ", "LT", "GT", 
                  "LTEQ", "GTEQ", "ASIGN", "LS", "RS", "LP", "RP", "LB", 
                  "RB", "SEMI", "COMMA", "IntLit", "FloatLit", "BoolLit", 
                  "StrLit", "COMMENT", "LINE_COMMENT", "WS", "ID", "ILLEGAL_ESCAPE", 
                  "UNCLOSE_STRING", "ERROR_CHAR" ]

    grammarFileName = "MC.g4"

    def __init__(self, input=None, output:TextIO = sys.stdout):
        super().__init__(input, output)
        self.checkVersion("4.7.2")
        self._interp = LexerATNSimulator(self, self.atn, self.decisionsToDFA, PredictionContextCache())
        self._actions = None
        self._predicates = None


    def emit(self):
        tk = self.type
        if tk == self.UNCLOSE_STRING:       
            result = super().emit();
            raise UncloseString(result.text);
        elif tk == self.ILLEGAL_ESCAPE:
            result = super().emit();
            raise IllegalEscape(result.text);
        elif tk == self.ERROR_CHAR:
            result = super().emit();
            raise ErrorToken(result.text); 
        else:
            return super().emit();


    def action(self, localctx:RuleContext, ruleIndex:int, actionIndex:int):
        if self._actions is None:
            actions = dict()
            actions[42] = self.StrLit_action 
            self._actions = actions
        action = self._actions.get(ruleIndex, None)
        if action is not None:
            action(localctx, actionIndex)
        else:
            raise Exception("No registered action for:" + str(ruleIndex))


    def StrLit_action(self, localctx:RuleContext , actionIndex:int):
        if actionIndex == 0:
            setText(getText().substring(1, getText().length()-1));
     


